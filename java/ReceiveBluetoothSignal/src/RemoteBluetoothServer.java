import javax.bluetooth.*;

/**
 * @author zhhsp
 *
 */
public class RemoteBluetoothServer {

	public static void main(String[] args) {
		
		LocalDevice localDevice;
		try {
			localDevice = LocalDevice.getLocalDevice();
		} catch (BluetoothStateException e) {
			e.printStackTrace();
			return;
		}
        System.out.println("My device address: "+localDevice.getBluetoothAddress());
        System.out.println("My device name: "+localDevice.getFriendlyName() + "\n");
        
		Thread waitThread = new Thread(new WaitThread());  
        waitThread.start(); 
	}

}
