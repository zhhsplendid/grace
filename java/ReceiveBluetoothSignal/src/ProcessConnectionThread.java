import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.InputStream;

import javax.microedition.io.*;


public class ProcessConnectionThread implements Runnable{

	private StreamConnection mConnection;

	// Constant that indicate command from devices
	private static final int KEY_LEFT = 0;
	private static final int KEY_RIGHT = 1;
	private static final int KEY_F5 = 2;
	private static final int EXIT_CMD = 3;
	
	public ProcessConnectionThread(StreamConnection connection)
	{
		mConnection = connection;
	}

	@Override
	public void run() {
		try {
			// prepare to receive data
			InputStream inputStream = mConnection.openInputStream();

			System.out.println("waiting for input");

			while (true) {
				int command = inputStream.read();
				System.out.println("command:" + command);
				if (command == EXIT_CMD)
				{
					System.out.println("Not recognized gesture");
				//	break;
				}
				
				if (command == -1) {
					System.out.println("Finish processor");
					break;
				}
				processCommand(command);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Process the command from client
	 * @param command the command code
	 */
	private void processCommand(int command) {
		try {
			Robot robot = new Robot();
			switch (command) {
	    		case KEY_RIGHT:
	    			robot.keyPress(KeyEvent.VK_RIGHT);
	    			System.out.println("Right");
	    			break;
	    		case KEY_LEFT:
	    			robot.keyPress(KeyEvent.VK_LEFT);
	    			System.out.println("Left");
	    			break;
	    		case KEY_F5:
	    			robot.keyPress(KeyEvent.VK_F5);
	    			System.out.println("F5");
	    			break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
