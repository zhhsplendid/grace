import javax.bluetooth.*;
import javax.microedition.io.*;

public class WaitThread implements Runnable{

	/** Constructor */
	public WaitThread() {
	}

	@Override
	public void run() {
		waitForConnection();
	}

	/** Waiting for connection from devices */
	private void waitForConnection() {
		// retrieve the local Bluetooth device object
		LocalDevice local = null;

		StreamConnectionNotifier notifier;
		StreamConnection connection = null;

		// setup the server to listen for connection
		try {
			local = LocalDevice.getLocalDevice();
			local.setDiscoverable(DiscoveryAgent.GIAC);

			UUID uuid = new UUID("1101", true); 
			String url = "btspp://localhost:" + uuid.toString() + ";name=RemoteBluetooth";
			System.out.println(url);
			notifier = (StreamConnectionNotifier)Connector.open(url);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
       	       	// waiting for connection
		//while(true) {
			try {
				System.out.println("waiting for connection...");
	            connection = notifier.acceptAndOpen();
	            RemoteDevice remoteDevice = RemoteDevice.getRemoteDevice(connection);
	            System.out.println("Remote device address: "+remoteDevice.getBluetoothAddress());
	            System.out.println("Remote device name: "+remoteDevice.getFriendlyName(true));
	            
				Thread processThread = new Thread(new ProcessConnectionThread(connection));
				processThread.start();
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
		//}
	}
}
