package fileParser;

import java.io.*;

public class FileParser {
	
	static final int MAX_ACC_LEN = 500;
	
	File file;
	BufferedWriter fout;
	BufferedReader fin; 
	
	public FileParser(String fileName) {
		file = new File(fileName);
	}
	
	public FileParser(String dir, String fileName) {
		if(dir == null) {
			file = new File(fileName);
		}
		else {
			file = new File(dir, fileName);
		}
	}
	
	public void close() {
		
		try {
			if(fout != null) {
				fout.flush();
				fout.close();
			}
			if(fin != null) {
				fin.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean writeln(String str) {
		return write(str + "\n");
	}
	
	public String readln() {
		String line = null;
		try {
			if(fin == null) {
				fin = new BufferedReader(new FileReader(file));
			}
			line = fin.readLine();
        } catch(IOException e) {
            e.printStackTrace();
        }
		return line;
	}
	
	public boolean write(String str) {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            
            if(fout == null) {
            	fout = new BufferedWriter(new FileWriter(file));
            }
            
            fout.write(str);
            fout.flush();
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
	
	public double[][] parser() {
		return parser(file);
	}
	
	public static double[][] parser(String dir, String fileName) {
		File file = new File(dir, fileName);
		return parser(file);
	}

	public static double[][] parser(File file) {
		
        double data[][] = new double[MAX_ACC_LEN][DataFiles.DIMENSION];
        int dCount = 0;
        try {
            BufferedReader fin = new BufferedReader(new FileReader(file));
            String line;
            String numberStrs[];
            while( (line = fin.readLine()) !=  null) {
                numberStrs = line.trim().split("\\s+");
                for(int i = 0; i < DataFiles.DIMENSION; ++i) {
                    data[dCount][i] = Double.valueOf(numberStrs[i]);
                }
                ++dCount;
            }
            fin.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
        double[][] ret = new double[dCount][DataFiles.DIMENSION];
        for(int i = 0; i < dCount; ++i) {
            for(int j = 0; j < DataFiles.DIMENSION; ++j) {
                ret[i][j] = data[i][j];
            }
        }
        return ret;
	}
	
}
