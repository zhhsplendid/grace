package fileParser;

import main.Main;
import java.io.*;
import utilities.*;

public class DataFiles {
	
	//see data/readme.txt for data format
	public static final int TOTAL_USERS = 8; // 1 ~ 8
	public static final int TOTAL_DAYS = 7; // 1 ~ 7
	public static final int TOTAL_GESTURES = 8; // 1 ~ 8
	public static final int TOTAL_REPEAT = 10; // 1 ~ 10
	
	public static final int TOTAL_FILES = TOTAL_USERS * TOTAL_DAYS * TOTAL_GESTURES * TOTAL_REPEAT;
	
	public static final int DIMENSION = 3;
	
	public static int offsetToIndex(int[] off) {
		Debug.check(off.length == 4);
		int index = off[0] - 1;
		index = index * TOTAL_DAYS + off[1] - 1;
		index = index * TOTAL_GESTURES + off[2] - 1;
		index = index * TOTAL_REPEAT + off[3] - 1;
		return index;
	}
	
	public static int offsetToIndex(int user, int day, int gest, int repeat) {
		int[] off = new int[4];
		off[0] = user;
		off[1] = day;
		off[2] = gest;
		off[3] = repeat;
		return offsetToIndex(off);
	}
	
	public static int[] getOffset(int index) {
		int[] ret = new int[4];
		int user = 1 + index / (TOTAL_DAYS * TOTAL_GESTURES * TOTAL_REPEAT);
		int day = 1 + index / (TOTAL_GESTURES * TOTAL_REPEAT) % TOTAL_DAYS;
		int gest = 1 + index / (TOTAL_REPEAT) % TOTAL_GESTURES;
		int rep = 1 + index % TOTAL_REPEAT;
		ret[0] = user;
		ret[1] = day;
		ret[2] = gest;
		ret[3] = rep;
		return ret;
	}
	
	public static boolean testIndexOffset() {
		int[] testcase = {1, 384, 508, 309, 847, 1134, 2579, 3674};
		boolean result = true;
		for(int i = 0; i < testcase.length; ++i) {
			int[] off = getOffset(testcase[i]);
			int rev = offsetToIndex(off);
			System.out.println(rev + " " + testcase[i]);
			if(rev != testcase[i]) {
				result = false;
			}
		}
		return result;
	}
	
	public static int[] indicesOfSameUserTrainData(int user, int templates[]) {
		int[] ret = new int[TOTAL_DAYS * TOTAL_GESTURES * templates.length];
		for(int d = 1; d <= TOTAL_DAYS; ++d) {
			int[] dayData = indicesOfSameUserSameDayTrainData(user, d, templates);
			System.arraycopy(dayData, 0, ret, (d - 1) * dayData.length, dayData.length);
		}
		return ret;
	}
	
	public static int[] indicesOfSameUserTestData(int user, int templates[]) {
		int[] ret = new int[TOTAL_DAYS * TOTAL_GESTURES * (TOTAL_REPEAT - templates.length)];
		for(int d = 1; d <= TOTAL_DAYS; ++d) {
			int[] dayData = indicesOfSameUserSameDayTestData(user, d, templates);
			System.arraycopy(dayData, 0, ret, (d - 1) * dayData.length, dayData.length);
		}
		return ret;
	}
	
	public static int[] indicesOfSameUserSameDayTrainData(int user, int day, int templates[]) {
		
		int[] ret = new int[TOTAL_GESTURES * templates.length];
		for(int i = 1; i <= TOTAL_GESTURES; ++i) {
			for(int j: templates) {
				ret[i - 1] = offsetToIndex(user, day, i, j);
			}
		}
		return ret;
	}
	
	public static int[] indicesOfSameUserSameDayTestData(int user, int day, int templates[]) {
		
		int[] ret = new int[TOTAL_GESTURES * (TOTAL_REPEAT - templates.length)];
		int count = 0;
		for(int i = 1; i <= TOTAL_GESTURES; ++i) {
			for(int j = 1; j <= TOTAL_REPEAT; ++j) {
				if(! Utilities.contains(templates, j)) {
					ret[count] = offsetToIndex(user, day, i, j);
					++count;
				}
			}
		}
		return ret;
	}
	
	public static double[][] readData(int index) {
		int[] off = getOffset(index);
		return readData(off[0], off[1], off[2], off[3]);
	}
	
	public static double[][] readData(int[] off) {
		return readData(off[0], off[1], off[2], off[3]);
	}
	
	public static double[][] readData(int user, int day, int gest, int rep) {
		String dir = Main.DATA_DIR + "U" + user + " (" + day + ")";
		File dirFile = new File(dir);
		
		File[] listFiles = dirFile.listFiles();
		for(File dataFile: listFiles) {
			if(dataFile.getName().endsWith( gest + "-" + rep + ".txt")) {
				return FileParser.parser(dataFile);
			}
		}
		return null;
	}
	
	
}
