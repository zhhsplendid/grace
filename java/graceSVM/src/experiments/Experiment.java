package experiments;

import fileParser.DataFiles;
import methods.*;
import utilities.Debug;
import utilities.Utilities;

/**
 * This class evaluates methods.
 * @author zhhsp
 *
 */
public class Experiment {
	
	/**
	 * We indexed data files from 0 ~ (DataFiles.TOTAL_FILES - 1)
	 */
	public int[] trainIndices = null;
	public int[] testIndices = null;
	
	ApproachMethod aMethod = null;
	
	public Experiment() {
		
	}
	
	public Experiment(ApproachMethod apm) {
		setMethod(apm);
	}
	
	public void setMethod(ApproachMethod apm) {
		aMethod = apm;
	}
	
	public void setDataByNumber(int trainNum, int testNum) {
		trainIndices = Utilities.continuousArray(0, trainNum - 1);
		testIndices = Utilities.continuousArray(trainNum, trainNum + testNum - 1);
	}
	
	public void setDataByFraction(double frac) {
		int trainNum = (int) (DataFiles.TOTAL_FILES * frac);
		int testNum = DataFiles.TOTAL_FILES - trainNum;
		
		trainIndices = Utilities.continuousArray(0, trainNum - 1);
		testIndices = Utilities.continuousArray(trainNum, trainNum + testNum - 1);
	}
	
	public void setCrossUserData(int numTemplate, int user) {
		int[] templates = Utilities.continuousArray(1, numTemplate);
		trainIndices = DataFiles.indicesOfSameUserTrainData(user, templates);
		testIndices = new int[DataFiles.TOTAL_FILES 
		        - DataFiles.TOTAL_GESTURES * DataFiles.TOTAL_REPEAT * DataFiles.TOTAL_DAYS];
		int copyBeginPos = 0;
		for(int otherUser = 1; otherUser <= DataFiles.TOTAL_USERS; ++otherUser) {
			int[] testTemp = new int[0];
			if(otherUser != user) {
				int[] testIndicesOtherUsers = DataFiles.indicesOfSameUserTestData(otherUser, testTemp);
				System.arraycopy(testIndicesOtherUsers, 0, testIndices, 
						copyBeginPos, testIndicesOtherUsers.length);
				copyBeginPos += testIndicesOtherUsers.length;
			}
		}
	}
	
	//As in uWave paper
	public void setIthTest(int user, int test) {
		int templateFromDay = test / DataFiles.TOTAL_REPEAT + 1;
		int templateFromRep = test % DataFiles.TOTAL_REPEAT + 1;
		trainIndices = new int[DataFiles.TOTAL_GESTURES];
		for(int g = 1; g <= DataFiles.TOTAL_GESTURES; ++g) {
			trainIndices[g - 1] = DataFiles.offsetToIndex(user, templateFromDay, g, templateFromRep);
		}
		testIndices = new int[DataFiles.TOTAL_DAYS * DataFiles.TOTAL_GESTURES * 
		                      DataFiles.TOTAL_REPEAT - DataFiles.TOTAL_GESTURES];
		int count = 0;
		for(int d = 1; d <= DataFiles.TOTAL_DAYS; ++d) {
			for(int g = 1; g <= DataFiles.TOTAL_GESTURES; ++g) {
				for(int r = 1; r <= DataFiles.TOTAL_REPEAT; ++r) {
					if(d != templateFromDay || r != templateFromRep) {
						testIndices[count++] = DataFiles.offsetToIndex(user, d, g, r);
					}
				}
			}
		}
	}
	
	public void setEveryUserData(int user, int templateNum) {
		int[] templates = Utilities.continuousArray(1, templateNum);
		trainIndices = DataFiles.indicesOfSameUserTrainData(user, templates);
		testIndices = DataFiles.indicesOfSameUserTestData(user, templates);
	}
	
	//As in uWave paper
	public void setEveryUserEveryDayData(int user, int day, int templateNum) {
		int[] templates = Utilities.continuousArray(1, templateNum);
		trainIndices = DataFiles.indicesOfSameUserSameDayTrainData(user, day, templates);
		testIndices = DataFiles.indicesOfSameUserSameDayTestData(user, day, templates);
		//Utilities.printlnArray(trainIndices);
		//Utilities.printlnArray(testIndices);
	}
	
	public double run() {
		Debug.check(aMethod != null, "You forget to set method");
		Debug.check(trainIndices != null, "You forget to set train set");
		Debug.check(testIndices != null, "You forget to set test set");
		aMethod.convertTrainData(trainIndices);
		aMethod.convertTestData(testIndices);
		aMethod.train();
		return aMethod.test();
	}
}
