package dtwlib;

import java.io.*;
import java.util.Arrays;

import main.*;

/**
 * Created by zhhsp on 10/24/2015.
 */
public class DTWLib {
//how to use this class:
//1. Set "recordFlag" to true to record gestures in a file as template. In this mode, input gestures will be written to files named "$index.umv"
//and become templates for recognition. The number of templates is fixed and defined as "NUM_TEMPLATES".
//Set "recordFlag" to false to enable recognition. Recognition result will be returned by endGesture() as the index of the template.
//
//2. When a gesture begins, call beginGesture(); when the gesture finishes, call endGesture();
//put the acc samples during a gesture into accBuffer like the following(x, y, z are the acceleration in g):
//	accBuffer[accIndex][0] = x EARTH_GRAVITY;
//	accBuffer[accIndex][1] = y EARTH_GRAVITY;
//	accBuffer[accIndex][2] = z EARTH_GRAVITY - ERATH_GRAVITY;
//	accIndex++;
//Make sure (accIndex < MAX_ACC_LEN)!!!!!!!!!!!!!!!!!!!!
//beginGesture() allocates memory and initializes variables; endGesture() record/recognize the input gesture and output results
//
    public boolean recordFlag = true;
    public static final double EARTH_GRAVITY = 10;

    public int NUM_TEMPLATES = 0;
    public static final String NUM_TEM_SAVE_FILE = "number_of_templates.uwv";
    public static final String SAVE_DIR = Main.TMP_DATA_DIR + "uWave/";
    
    public static final int DIMENSION = 3;
    public static final int QUAN_WIN_SIZE = 8;
    public static final int QUAN_MOV_STEP = 4;
    public static final int MAX_ACC_LEN = 500;

    public double accBuffer[][];
    public int accIndex;

    
    
    public void recordMode() {
        recordFlag = true;
    }

    public void detectMode() {
        recordFlag = false;
    }

    public int clearGestures() {
        int deletedFiles = 0;

        File dir = new File(SAVE_DIR);
        for(File file: dir.listFiles()) {
            ++deletedFiles;
            file.delete();
        }
        
        NUM_TEMPLATES = 0;
        return deletedFiles;
    }

    public void beginGesture() {
        accBuffer = new double[MAX_ACC_LEN][DIMENSION];
        accIndex = 0;
    }

    public void addAccerelation(double acce[]) {
        //We ignore too many accerelations.
        if(accIndex < MAX_ACC_LEN) {
            for (int i = 0; i < DIMENSION; ++i) {
                accBuffer[accIndex][i] = acce[i];
            }
            ++accIndex;
        }
    }

    public int endGesture() {
        int ret = -1;
        if(recordFlag) {
            accIndex = quantizeAcc(accBuffer, accIndex);
            writeFile(accBuffer, accIndex, NUM_TEMPLATES);
            ++NUM_TEMPLATES;
            ret = NUM_TEMPLATES;
        }
        else {
            accIndex = quantizeAcc(accBuffer, accIndex);
            Gesture templates[] = new Gesture[NUM_TEMPLATES];
            for(int i = 0; i < NUM_TEMPLATES; ++i) {
                templates[i] = readFile(i);
                //templates[i].length = quantizeAcc(templates[i].data, templates[i].length);
            }
            ret = detectGesture(accBuffer, accIndex, templates, NUM_TEMPLATES);
        }
        return ret;
    }
    
    public int[] kNearestDetectResult(int k) {
    	
    	accIndex = quantizeAcc(accBuffer, accIndex);
        Gesture templates[] = new Gesture[NUM_TEMPLATES];
        for(int i = 0; i < NUM_TEMPLATES; ++i) {
            templates[i] = readFile(i);
            //templates[i].length = quantizeAcc(templates[i].data, templates[i].length);
        }
        return detectGestureKNearest(accBuffer, accIndex, templates, NUM_TEMPLATES, k);

    }
    
    /**
     * A class used for sort
     */
    class DisAndPos implements Comparable<DisAndPos>{
    	public double dis;
    	public int pos;
    	
    	public DisAndPos() {
    		dis = 0;
    		pos = 0;
    	}
    	
    	public DisAndPos(double d, int p) {
    		dis = d;
    		pos = p;
    	}

		@Override
		public int compareTo(DisAndPos dap) {
			if(dis < dap.dis) {
				return -1;
			}
			else if(dis > dap.dis) {
				return 1;
			}
			else {
				return 0;
			}
		}
    	
    	
    }
    
    public int[] detectGestureKNearest(double input[][], int length, Gesture templates[], int templateNum, int k) {
    	if( length <= 0)
            return new int[k];
    	
        int[] ret = new int[k];
        
        DisAndPos[] disAndPos = new DisAndPos[NUM_TEMPLATES];
        double distance;
        
        double table[];
        for(int i = 0; i < templateNum; ++i) {
            table = new double[length * templates[i].length];
            for(int j = 0; j < table.length; ++j) {
                table[j] = -1;
            }
            distance = DTWdistance(input, length, templates[i].data, templates[i].length, length - 1, templates[i].length - 1, table);
            distance /= (length + templates[i].length);
            disAndPos[i] = new DisAndPos(distance, i);
        }
        
        Arrays.sort(disAndPos);
        
        for(int i = 0; i < k; ++i) {
            ret[i] = disAndPos[i].pos;
        }
        return ret;
    }
    
    int writeFile(double data[][], int length, int index) {
        String fileName = Integer.toString(index) + ".uwv";

        try {
            //FileOutputStream fos = openFileOutput(fileName, Context.MODE_PRIVATE);
            File file = new File(SAVE_DIR, fileName);
            if (!file.exists()) {
                file.createNewFile();
            }

            BufferedWriter fout = new BufferedWriter(new FileWriter(file));
            for(int i = 0; i < length; ++i) {
                for(int j = 0; j < DIMENSION; ++j) {
                    fout.write(data[i][j] + " ");
                }
                fout.newLine();
            }
            fout.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    Gesture readFile(int index) {
        String fileName = Integer.toString(index) + ".uwv";
        double data[][] = new double[MAX_ACC_LEN][DIMENSION];
        int dCount = 0;
        try {
            File file = new File(SAVE_DIR, fileName);
            BufferedReader fin = new BufferedReader(new FileReader(file));
            String line;
            String numberStrs[];
            while( (line = fin.readLine()) !=  null) {
                numberStrs = line.trim().split("\\s+");
                for(int i = 0; i < DIMENSION; ++i) {
                    data[dCount][i] = Double.valueOf(numberStrs[i]);
                }
                ++dCount;
            }
            fin.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
        Gesture ret = new Gesture(dCount, DIMENSION);
        for(int i = 0; i < dCount; ++i) {
            for(int j = 0; j < DIMENSION; ++j) {
                ret.data[i][j] = data[i][j];
            }
        }
        return ret;
    }
    
    public static double[][] winStepQuantize(double accData[][], int window, int step) {
    	int i = 0, j, k = 0, l, length = accData.length;
    	double sum;
        double temp[][] = new double[length / step + 1][DIMENSION];
        
        while(i < length) {
            if( i + window > length)
                window = length - i;
            for( l = 0; l < DIMENSION; l++) {
                sum = 0;
                for( j = i; j < window + i; j++)
                    sum += accData[j][l];
                temp[k][l] = (sum / window);
            }
            k++;
            i += step;
        }
        return temp;
    }
    
    public static double[][] interestingQuantize(double accData[][], int window, int step) {
    	int i = 0, j, k = 0, l, length = accData.length;
    	double sum;
        double temp[][] = new double[length / step + 1][DIMENSION];
        
        while(i < length) {
            if( i + window > length)
                window = length - i;
            for( l = 0; l < DIMENSION; l++) {
                sum = 0;
                for( j = i; j < window + i; j++)
                    sum += accData[j][l];
                temp[k][l] = (sum / window);
            }
            k++;
            i += step;
        }
        
        double ret[][] = new double[k][DIMENSION];
        for( i = 0; i < k; i++)
            for( l = 0; l < DIMENSION; l++) {
                if( temp[i][l] > 10 ) {
                    if( temp[i][l] > 20)
                        temp[i][l] = 16;
                    else
                        temp[i][l] = 11 + (int)((temp[i][l] - 10) * 5 / 10);
                } else if( temp[i][l] < -10) {
                    if( temp[i][l] < -20)
                        temp[i][l] = -16;
                    else
                        temp[i][l] = -10 + (int)((temp[i][l] + 10) * 5 / 10);
                }
                ret[i][l] = temp[i][l];
            }
            
        return ret;
    }
    
    
    /**
     * Spcific QUAN_WIN_SIZE and QUAN_MOV_STEP
     * @param accData
     * @param window
     * @param step
     * @return
     */
    public static double[][] debugCodeQuantizeAcc(double accData[][], int window, int step) {
    	int i = 0, j, k = 0, l, length = accData.length;
    	double sum;
        double temp[][] = new double[length / step + 1][DIMENSION];
        
        while(i < length) {
            if( i + window > length)
                window = length - i;
            for( l = 0; l < DIMENSION; l++) {
                sum = 0;
                for( j = i; j < window + i; j++)
                    sum += accData[j][l];
                temp[k][l] = (sum / window) * EARTH_GRAVITY;
            }
            k++;
            i += step;
        }
        
        double ret[][] = new double[k][DIMENSION];
        for( i = 0; i < k; i++)
            for( l = 0; l < DIMENSION; l++) {
                if( temp[i][l] > 10 ) {
                    if( temp[i][l] > 20)
                        temp[i][l] = 16;
                    else
                        temp[i][l] = 11 + (int)((temp[i][l] - 10) * 5 / 10);
                } else if( temp[i][l] < -10) {
                    if( temp[i][l] < -20)
                        temp[i][l] = -16;
                    else
                        temp[i][l] = -10 + (int)((temp[i][l] + 10) * 5 / 10);
                }
                ret[i][l] = temp[i][l];
            }
        return ret;
    }
    
    public static double[][] theirCodeQuantizeAcc(double accData[][], int window, int step) {
    	int i = 0, j, k = 0, l, length = accData.length;
    	double sum;
        double temp[][] = new double[length / step + 1][DIMENSION];
        
        while(i < length) {
            if( i + window > length)
                window = length - i;
            for( l = 0; l < DIMENSION; l++) {
                sum = 0;
                for( j = i; j < window + i; j++)
                    sum += accData[j][l];
                temp[k][l] = (sum / window) * EARTH_GRAVITY;
            }
            k++;
            i += step;
        }
        
        double ret[][] = new double[k][DIMENSION];
        for( i = 0; i < k; i++)
            for( l = 0; l < DIMENSION; l++) {
                if( temp[i][l] > 10 ) {
                    if( temp[i][l] > 20)
                        temp[i][l] = 16;
                    else
                        temp[i][l] = 11 + (int)((temp[i][l] - 10) / 10 * 5);
                } else if( temp[i][l] < -10) {
                    if( temp[i][l] < -20)
                        temp[i][l] = -16;
                    else
                        temp[i][l] = -10 + (int)((temp[i][l] + 10) / 10 * 5);
                }
                ret[i][l] = temp[i][l];
            }
        return ret;
    }
    
    public static double[][] paperQuantizeAcc(double accData[][], int window, int step) {
    	int i = 0, j, k = 0, l, length = accData.length;
    	double sum;
        double temp[][] = new double[length / step + 1][DIMENSION];
        
        while(i < length) {
            if( i + window > length)
                window = length - i;
            for( l = 0; l < DIMENSION; l++) {
                sum = 0;
                for( j = i; j < window + i; j++)
                    sum += accData[j][l];
                temp[k][l] = (sum / window) * EARTH_GRAVITY;
            }
            k++;
            i += step;
        }
        
        double ret[][] = new double[k][DIMENSION];
        for( i = 0; i < k; i++)
            for( l = 0; l < DIMENSION; l++) {
                if( temp[i][l] > 10 ) {
                    if( temp[i][l] > 20)
                        temp[i][l] = 16;
                    else
                        temp[i][l] = 11 + (int)((temp[i][l] - 10) / 10 * 5);
                } else if( temp[i][l] < -10) {
                    if( temp[i][l] < -20)
                        temp[i][l] = -16;
                    else
                        temp[i][l] = -10 + (int)((temp[i][l] + 10) / 10 * 5);
                } else if( temp[i][l] > 0) {
                	temp[i][l] = Math.ceil(temp[i][l]);
                } else if( temp[i][l] < 0) {
                	temp[i][l] = Math.floor(temp[i][l]);
                }
                ret[i][l] = temp[i][l];
            }
        return ret;
    }
    
    public static int quantizeAcc(double accData[][], int length) {
    	
        double[][] tmp = paperQuantizeAcc(accData, QUAN_WIN_SIZE, QUAN_MOV_STEP);
        //accData = tmp;
        for(int l = 0; l < tmp.length; ++l) {
        	System.arraycopy(tmp[l], 0, accData[l], 0, DIMENSION);
        }
        return tmp.length;
    	/*
    	int i = 0, j, k = 0, l, window = QUAN_WIN_SIZE;
        double sum;
        double temp[][] = new double[length / QUAN_MOV_STEP + 1][DIMENSION];
        //take moving window average

        while(i < length) {
            if( i + window > length)
                window = length - i;
            for( l = 0; l < DIMENSION; l++) {
                sum = 0;
                for( j = i; j < window + i; j++)
                    sum += accData[j][l];
                temp[k][l] = sum / window;
            }
            k++;
            i += QUAN_MOV_STEP;
        }//while
        //nonlinear quantization and copy quantized value to original buffer
        for( i = 0; i < k; i++)
            for( l = 0; l < DIMENSION; l++) {
                if( temp[i][l] > 10 ) {
                    if( temp[i][l] > 20)
                        temp[i][l] = 16;
                    else
                        temp[i][l] = 11 + (int)((temp[i][l] - 10) * 5 / 10);
                } else if( temp[i][l] < -10) {
                    if( temp[i][l] < -20)
                        temp[i][l] = -16;
                    else
                        temp[i][l] = -10 + (int)((temp[i][l] + 10) * 5 / 10);
                }
                accData[i][l] = temp[i][l];
            }
        return k;
        */
    }

    public int detectGesture(double input[][], int length, Gesture templates[], int templateNum) {


        if( length <= 0)
            return -1;
        int ret = 0;

        double distances[] = new double [NUM_TEMPLATES];
        //int table[MAX_ACC_LEN/QUAN_MOV_STEP*MAX_ACC_LEN/QUAN_MOV_STEP];
        double table[];
        for(int i = 0; i < templateNum; ++i) {
            table = new double[length * templates[i].length];
            for(int j = 0; j < table.length; ++j) {
                table[j] = -1;
            }
            distances[i] = DTWdistance(input, length, templates[i].data, templates[i].length, length - 1, templates[i].length - 1, table);
            distances[i] /= (length + templates[i].length);
        }

        for(int i = 1; i < templateNum; i++) {
            if( distances[i] < distances[ret]) {
                ret = i;
            }
        }
        return ret;

    }
    
    static double DTWdistance(double sample1[][], int length1, double sample2[][], int length2, int i, int j, double table[]) {

        if( i < 0 || j < 0)
            return 100000000;
        int tableWidth = length2;
        double localDistance = 0;
        int k;
        for( k = 0; k < DIMENSION; k++)
            localDistance += ((sample1[i][k] - sample2[j][k]) * (sample1[i][k] - sample2[j][k]));

        double sdistance, s1, s2, s3;

        if( i == 0 && j == 0) {
            if( table[i * tableWidth + j] < 0)
                table[i * tableWidth + j] = localDistance;
            return localDistance;
        } else if( i == 0) {
            if( table[i * tableWidth + (j-1)] < 0)
                sdistance = DTWdistance(sample1, length1, sample2, length2, i, j-1, table);
            else
                sdistance = table[i * tableWidth + j-1];
        } else if( j == 0) {
            if( table[(i - 1) * tableWidth + j] < 0)
                sdistance = DTWdistance(sample1, length1, sample2, length2, i-1, j, table);
            else
                sdistance = table[(i-1)*tableWidth+j];
        } else {
            if( table[i * tableWidth + (j - 1)] < 0)
                s1 = DTWdistance(sample1, length1, sample2, length2, i, j-1, table);
            else
                s1 = table[i*tableWidth+(j-1)];
            if( table[(i-1)*tableWidth+ j] < 0)
                s2 = DTWdistance(sample1, length1, sample2, length2, i-1, j, table);
            else
                s2 = table[(i-1)*tableWidth+ j];
            if( table[(i-1)*tableWidth+ j-1] < 0)
                s3 = DTWdistance(sample1, length1, sample2, length2, i-1, j-1, table);
            else
                s3 = table[(i-1)*tableWidth+ j-1];
            sdistance = s1 < s2 ? s1:s2;
            sdistance = sdistance < s3 ? sdistance:s3;
        }
        table[i * tableWidth + j] = localDistance + sdistance;
        return table[i * tableWidth + j];
    }
    
    public static double DTWdistance(double sample1[][], double sample2[][]) {
    	
    	double[] table = new double[sample1.length * sample2.length];
        for(int j = 0; j < table.length; ++j) {
            table[j] = -1;
        }
    	double distance = DTWdistance(sample1, sample1.length, sample2, sample2.length, sample1.length - 1, sample2.length - 1, table);
    	distance = distance / (sample1.length + sample2.length); //normalize
    	return distance;
    }
}
