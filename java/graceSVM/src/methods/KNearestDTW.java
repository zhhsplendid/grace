package methods;

import dtwlib.DTWLib;
import fileParser.DataFiles;
import fileParser.FileParser;
import main.Main;

public class KNearestDTW extends UWave {
	
	//K = 1 is equivalent to UWave
	int K = 1;
	
	private void setFileNames() {
		TRAIN_FILE = "kNearestDtw_train.gsvm";
		TEST_FILE = "kNearestDtw_test.gsvm";
		OUTPUT_FILE = "kNearestDtw_out.gsvm";
	}
	
	public KNearestDTW() {
		super();
		setFileNames();
	}
	
	public KNearestDTW(int k) {
		super();
		setFileNames();
		K = k;
	}
	
	public int getK() {
		return K;
	}
	
	public int setK(int k) {
		K = k;
		return K;
	}
	
	@Override
	public double test() {
		dtwLib.detectMode();
		
		FileParser file = new FileParser(Main.TMP_DATA_DIR, TEST_FILE);
		FileParser output = new FileParser(OUTPUT_FILE);
		String line = file.readln();
		String[] numStr = line.trim().split("\\s+");
		
		for(String str: numStr) {
			int index = Integer.valueOf(str);
			double[][] data = DataFiles.readData(index);
			
			dtwLib.beginGesture();
			for(int i = 0; i < data.length; ++i) {
				dtwLib.addAccerelation(data[i]);
			}
			int[] predictTemplates = dtwLib.kNearestDetectResult(K);
			
			//off is 1 ~ DataFiles.TOTAL_GESTURES
			int[] voteGests = new int[DataFiles.TOTAL_GESTURES + 1];
			for(int i = 0; i < K; ++i) {
				int predictIndex = trainIndices[predictTemplates[i]];
				int[] off = DataFiles.getOffset(predictIndex);
				int vote = off[2]; //off is 1 ~ DataFiles.TOTAL_GESTURES
				
				++voteGests[vote];
			}
			
			int resultGest = 0;
			for(int i = 1; i <= DataFiles.TOTAL_GESTURES; ++i) {
				if(voteGests[i] > voteGests[resultGest]) {
					resultGest = i;
				}
			}

			output.writeln(Integer.toString(resultGest));
		}
		file.close();
		output.close();
		System.out.println("Testing over");
		return accuracy();
	}
	
	@Override
	public String toString() {
		return "K=" + K + "_NearestDTW";
	}

}
