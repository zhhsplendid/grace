package methods;

import fileParser.FileParser;
import main.Main;

public abstract class ApproachMethod {
	
	public String TRAIN_FILE = "train.gsvm";
	public String TEST_FILE = "test.gsvm";
	public String OUTPUT_FILE = "out.gsvm";
	/**
	 * 
	 * @param indices indicate data to convert. The index format is in DataFiles.java
	 * @return file name of the converted data.
	 */
	//abstract String convertData(int[] indices, String fileName);
	
	abstract public String convertTrainData(int[] indices);
	abstract public String convertTestData(int[] indices);
	
	public abstract void train();
	public abstract double test();
	public abstract String toString();
	
	double accuracy() {
		FileParser predictFile = new FileParser(OUTPUT_FILE);
		FileParser groundTruthFile = new FileParser(Main.TMP_DATA_DIR, TEST_FILE);
		
		int rightCount = 0;
		int totalCount = 0;
		String line;
		String[] numStr;
		while((line = predictFile.readln()) != null) {
			numStr = line.trim().split("\\s+");

			double doubleLabel = Double.valueOf(numStr[0]);
			int predictLabel = (int)Math.round(doubleLabel);
			
			line = groundTruthFile.readln();
			if(line == null) {
				break;
			}
			++totalCount;
			numStr = line.trim().split("\\s+");
			doubleLabel = Double.valueOf(numStr[0]);
			int groundTruth = (int)Math.round(doubleLabel);
			
			if(groundTruth == predictLabel) {
				++rightCount;
			}
		}
		
		predictFile.close();
		groundTruthFile.close();
		double ret = ((double) rightCount) / (double)(totalCount);
		System.out.println("Total: " + totalCount + ", right: " + rightCount + ", accuracy: " + ret);
		return ret; 
	}
}
