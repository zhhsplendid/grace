package methods;

import dtwlib.DTWLib;

public class GDTWKernel extends DTWKernel {
	
	double GARMMA = 0.5;
	
	public GDTWKernel() {
		setGarmma(0.5);
		TRAIN_FILE = GARMMA + "_gdtw_kernel_train.gsvm";
		TEST_FILE = GARMMA + "_gdtw_kernel_test.gsvm";
		OUTPUT_FILE = GARMMA + "_gdtw_kernel_out.gsvm";
	}
	
	public GDTWKernel(double garmma) {
		setGarmma(garmma);
		TRAIN_FILE = GARMMA + "_gdtw_kernel_train.gsvm";
		TEST_FILE = GARMMA + "_gdtw_kernel_test.gsvm";
		OUTPUT_FILE = GARMMA + "_gdtw_kernel_out.gsvm";
	}
	
	public double getGarmma() {
		return GARMMA;
	}
	
	public void setGarmma(double garmma) {
		GARMMA = garmma;
	}
	
	@Override
	double kernel(double data1[][], double data2[][]) {
		return Math.exp( - GARMMA * DTWLib.DTWdistance(data1, data2));
	}
	
	@Override
	public String toString() {
		return "GDTWKernel_" + GARMMA;
	}
}
