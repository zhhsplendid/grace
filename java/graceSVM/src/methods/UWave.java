package methods;

import dtwlib.DTWLib;
import fileParser.DataFiles;
import fileParser.FileParser;
import main.Main;

/**
 * This uWave isn't original uWave. I changed quantize function
 * @author zhhsp
 *
 */
public class UWave extends ApproachMethod{
	
	DTWLib dtwLib;
	int[] trainIndices;
	
	public UWave() {
		TRAIN_FILE = "uwave_train.gsvm";
		TEST_FILE = "uwave_test.gsvm";
		OUTPUT_FILE = "uwave_out.gsvm";
		dtwLib = new DTWLib();
	}
	
	/**
	 * In uWave, we quantize data here
	 */
	
	String convertData(int[] indices, String fileName) {
		FileParser file = new FileParser(Main.TMP_DATA_DIR, fileName);
		String str = "";
		for(int i: indices) {
			str = str + i + " ";
		}
		file.write(str);
		return fileName;
	}
	
	@Override
	public String convertTrainData(int[] indices) {
		System.out.println("converting train data");
		trainIndices = indices;
		return convertData(indices, TRAIN_FILE);
	}
	
	@Override
	public String convertTestData(int[] indices) {
		System.out.println("converting test data");
		return convertData(indices, TEST_FILE);
	}
	
	@Override
	public void train() {
		dtwLib.clearGestures();
		dtwLib.recordMode();
		
		FileParser file = new FileParser(Main.TMP_DATA_DIR, TRAIN_FILE);
		String line = file.readln();
		String[] numStr = line.trim().split("\\s+");
		
		for(String str: numStr) {
			int index = Integer.valueOf(str);
			double[][] data = DataFiles.readData(index);
			
			dtwLib.beginGesture();
			for(int i = 0; i < data.length; ++i) {
				dtwLib.addAccerelation(data[i]);
			}
			dtwLib.endGesture();
		}
		System.out.println("Training over");
	}
	
	@Override
	public double test() {
		dtwLib.detectMode();
		
		FileParser file = new FileParser(Main.TMP_DATA_DIR, TEST_FILE);
		FileParser output = new FileParser(OUTPUT_FILE);
		String line = file.readln();
		String[] numStr = line.trim().split("\\s+");
		
		for(String str: numStr) {
			int index = Integer.valueOf(str);
			double[][] data = DataFiles.readData(index);
			
			dtwLib.beginGesture();
			for(int i = 0; i < data.length; ++i) {
				dtwLib.addAccerelation(data[i]);
			}
			int predictTemplate = dtwLib.endGesture();
			int predictIndex = trainIndices[predictTemplate];
			int[] off = DataFiles.getOffset(predictIndex);
			output.writeln(Integer.toString(off[2]));
		}
		file.close();
		output.close();
		System.out.println("Testing over");
		return accuracy();
	}
	
	@Override
	double accuracy() {
		FileParser predictFile = new FileParser(OUTPUT_FILE);
		FileParser testIndices = new FileParser(Main.TMP_DATA_DIR, TEST_FILE);
		
		int rightCount = 0;
		int totalCount = 0;
		
		String line = testIndices.readln();
		String[] numStr = line.trim().split("\\s+");
		int i = 0;
		while((line = predictFile.readln()) != null) {
			int predict = Integer.valueOf(line);
			
			int index = Integer.valueOf(numStr[i]);
			int[] off = DataFiles.getOffset(index);
			int groundTruth = off[2];
			
			++totalCount;
			if(groundTruth == predict) {
				++rightCount;
			}
			++i;
		}
		
		predictFile.close();
		testIndices.close();
		double ret = ((double) rightCount) / (double)(totalCount);
		System.out.println("Total: " + totalCount + ", right: " + rightCount + ", accuracy: " + ret);
		return ret; 
	}
	
	@Override
	public String toString() {
		return "UWave";
	}
}
