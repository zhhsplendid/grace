package methods;

import java.io.IOException;

import fileParser.*;
import main.*;

public class NaiveSegmentation extends ApproachMethod{
	
	
	
	public static final int NUMBER_SEG = 9;
	
	public NaiveSegmentation() {
		TRAIN_FILE = "naive_seg_train.gsvm";
		TEST_FILE = "naive_seg_test.gsvm";
		OUTPUT_FILE = "naive_seg_out.gsvm";
	}
	
	
	String convertData(int[] indices, String fileName) {
		FileParser file = new FileParser(Main.TMP_DATA_DIR, fileName);
		for(int index: indices) {
			int[] off = DataFiles.getOffset(index);
			double[][] data = DataFiles.readData(off);
			int segLength = data.length / (NUMBER_SEG + 1);
			if(segLength == 0){
				double[][] tmpData = new double[NUMBER_SEG + 1][DataFiles.DIMENSION];
				segLength = 1;
				for(int d = 0; d < DataFiles.DIMENSION; ++d) {
					for(int i = 0; i < data.length; ++i) {
						tmpData[i][d] = data[i][d];
					}
				}		
				data = tmpData;
			}
			int label = off[2];
			String str = Integer.toString(label);
			//double[][] toWrite = new double[NUMBER_SEG][DataFiles.DIMENSION];
			for(int i = 0; i < NUMBER_SEG; ++i) {
				for(int d = 0; d < DataFiles.DIMENSION; ++d) {
					int beginPos = i * segLength;
					int endPos = beginPos + 2 * segLength;
				
					double avg = 0;
				
					for(int j = beginPos; j < endPos; ++j) {
						avg += data[j][d];
					}
					avg = avg / (2 * segLength);
					//toWrite[i][d] = avg;
					str = str + " " + (i * DataFiles.DIMENSION + d + 1) + ":" + avg;
				}
			}
			file.writeln(str);
		}
		file.close();
		return fileName;
	}
	
	@Override
	public String convertTrainData(int[] indices) {
		System.out.println("converting train data");
		return convertData(indices, TRAIN_FILE);
	}
	@Override
	public String convertTestData(int[] indices) {
		System.out.println("converting test data");
		return convertData(indices, TEST_FILE);
	}
	
	@Override
	public void train() {
		System.out.println("training");
		String[] commandArgv = {Main.TMP_DATA_DIR + TRAIN_FILE};
		try {
			svm_train.main(commandArgv);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public double test() {
		String modelFile = TRAIN_FILE + ".model";
		String[] commandArgv = {Main.TMP_DATA_DIR + TEST_FILE, modelFile, OUTPUT_FILE};
		try {
			svm_predict.main(commandArgv);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println("accuracy = " + accuracy());
		return accuracy();
	}

	@Override
	public String toString() {
		return "NaiveSegmentation";
	}

}
