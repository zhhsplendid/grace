package methods;

import java.io.IOException;

import dtwlib.DTWLib;
import fileParser.DataFiles;
import fileParser.FileParser;
import main.Main;
import main.svm_predict;
import main.svm_train;

/**
 * In experiment, we found this method is not good. Try to use NDTW kernel
 * @author zhhsp
 *
 */

public class DTWKernel extends ApproachMethod {
	
	int[] trainIndices;
	
	public DTWKernel() {
		//Debug.check(false, "In experiment, we found this method is not good. Try to use NDTW kernel");
		TRAIN_FILE = "dtw_kernel_train.gsvm";
		TEST_FILE = "dtw_kernel_test.gsvm";
		OUTPUT_FILE = "dtw_kernel_out.gsvm";
	}
	
	double kernel(double data1[][], double data2[][]) {
		return DTWLib.DTWdistance(data1, data2);
	}
	
	double[][][] readDataFromFile(int[] indices) {
		int len = indices.length;
		double[][][] data = new double[len][][];
		for(int count = 0; count < len; ++count) {
			int i = indices[count];
			data[count] = DataFiles.readData(i);
		}
		return data;
	}
	
	String convertPreComputeKernel(int[] indices1, int[] indices2, String fileName) {
		FileParser file = new FileParser(Main.TMP_DATA_DIR, fileName);
		int len1 = indices1.length;
		int len2 = indices2.length;
		double[][][] data1 = readDataFromFile(indices1);
		double[][][] data2 = readDataFromFile(indices2);
		for(int count1 = 0; count1 < len1; ++count1) {
			System.out.println("converting " + count1 + ", total " + len1);
			int i1 = indices1[count1];
			int[] off1 = DataFiles.getOffset(i1);
			
			String str = Integer.toString(off1[2]); //label
			str = str + " 0:" + (count1 + 1);
			
			for(int count2 = 0; count2 < len2; ++count2) {
				
				double kernelValue = kernel(data1[count1], data2[count2]);
				str = str + " " + (count2 + 1) + ":" + kernelValue;
			}
			file.writeln(str);
		}
		file.close();
		return fileName;
	}
	
	@Override
	public String convertTrainData(int[] indices) {
		trainIndices = indices;
		return convertPreComputeKernel(indices, indices, TRAIN_FILE);
	}
	
	@Override
	public String convertTestData(int[] indices) {
		return convertPreComputeKernel(indices, trainIndices, TEST_FILE);
	}
	
	@Override
	public double test() {
		String modelFile = TRAIN_FILE + ".model";
		String[] commandArgv = {Main.TMP_DATA_DIR + TEST_FILE, modelFile, OUTPUT_FILE};
		try {
			svm_predict.main(commandArgv);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println("accuracy = " + accuracy());
		return accuracy();
	}

	@Override
	public void train() {
		System.out.println("training");
		String[] commandArgv = {"-t", "4", Main.TMP_DATA_DIR + TRAIN_FILE};
		try {
			svm_train.main(commandArgv);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString() {
		return "DTWKernel";
	}
}
