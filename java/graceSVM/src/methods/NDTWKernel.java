/**
 * 
 */
package methods;

import dtwlib.DTWLib;

/**
 * @author zhhsp
 *
 */
public class NDTWKernel extends DTWKernel {
	
	public NDTWKernel() {
		TRAIN_FILE = "ndtw_kernel_train.gsvm";
		TEST_FILE = "ndtw_kernel_test.gsvm";
		OUTPUT_FILE = "ndtw_kernel_out.gsvm";
	}
	
	@Override
	double kernel(double data1[][], double data2[][]) {
		return -DTWLib.DTWdistance(data1, data2);
	}
	
	@Override
	public String toString() {
		return "NDTWKernel";
	}
}
