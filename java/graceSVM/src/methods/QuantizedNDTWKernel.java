package methods;

import dtwlib.DTWLib;

public class QuantizedNDTWKernel extends QuantizedDTWKernel {
	
	public QuantizedNDTWKernel() {
		TRAIN_FILE = "quan_ndtw_train.gsvm";
		TEST_FILE = "quan_ndtw_test.gsvm";
		OUTPUT_FILE = "quan_ndtw_out.gsvm";
	}
	
	@Override
	double kernel(double data1[][], double data2[][]) {
		return -DTWLib.DTWdistance(data1, data2);
	}
	
	@Override
	public String toString() {
		return "QuantizedNDTWKernel";
	}
}
