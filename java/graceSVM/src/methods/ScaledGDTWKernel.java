package methods;

import dtwlib.DTWLib;

public class ScaledGDTWKernel extends ScaledNDTWKernel {
	
	double GARMMA = 0.5;
	
	public ScaledGDTWKernel() {
		setGarmma(0.5);
		TRAIN_FILE = "scaled_gdtw_train.gsvm";
		TEST_FILE = "scaled_gdtw_test.gsvm";
		OUTPUT_FILE = "scaled_gdtw_out.gsvm";
	}
	
	public ScaledGDTWKernel(double garmma) {
		setGarmma(garmma);
		TRAIN_FILE = "scaled_gdtw_train.gsvm";
		TEST_FILE = "scaled_gdtw_test.gsvm";
		OUTPUT_FILE = "scaled_gdtw_out.gsvm";
	}
	
	public double getGarmma() {
		return GARMMA;
	}
	
	public void setGarmma(double garmma) {
		GARMMA = garmma;
	}
	
	double kernel(double data1[][], double data2[][]) {
		return -DTWLib.DTWdistance(data1, data2);
	}
	
	@Override
	public String toString() {
		return "ScaledGDTW_" + GARMMA;
	}
}
