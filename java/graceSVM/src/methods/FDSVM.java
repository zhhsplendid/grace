package methods;

import java.io.IOException;

import fileParser.DataFiles;
import fileParser.FileParser;
import main.Main;
import main.svm_predict;
import main.svm_scale;
import main.svm_train;
import utilities.Debug;
import utilities.Utilities;

public class FDSVM extends NaiveSegmentation {
	
	final double NEAR_ZERO = 1e-6;
	final double LINEAR_COR_PARA = 0.001;
	final String SCALE_FILE = "fdsvm_scale.gsvm";
	final double SCALE_LOWER = -1;
	final double SCALE_UPPER = 1;
	public FDSVM() {
		TRAIN_FILE = "fdsvm_train.gsvm";
		TEST_FILE = "fdsvm_test.gsvm";
		OUTPUT_FILE = "fdsvm_out.gsvm";
	}
	
	//See FDSVM paper
	String convertData(int[] indices, String fileName) {
		FileParser file = new FileParser(Main.TMP_DATA_DIR, fileName);
		for(int index: indices) {
			int[] off = DataFiles.getOffset(index);
			double[][] data = DataFiles.readData(off);
			int segLength = data.length / (NUMBER_SEG + 1);
			
			if(segLength == 0){
				double[][] tmpData = new double[NUMBER_SEG + 1][DataFiles.DIMENSION];
				segLength = 1;
				for(int d = 0; d < DataFiles.DIMENSION; ++d) {
					for(int i = 0; i < data.length; ++i) {
						tmpData[i][d] = data[i][d];
					}
				}		
				data = tmpData;
			}
			int label = off[2];
			String str = Integer.toString(label);
			
			double[][][] toWrite = new double[DataFiles.DIMENSION][5][NUMBER_SEG];
			
			int count = 0;
			for(int i = 0; i < NUMBER_SEG; ++i) {
				//double[][][] saveR = new double[DataFiles.DIMENSION][][];
				int beginPos = i * segLength;
				//int endPos = beginPos + 2 * segLength;
				for(int d = 0; d < DataFiles.DIMENSION; ++d) {
					
					double[][] r = new double[4][2 * segLength];
					Utilities.twoDimensionCopy(data, beginPos, d, r[0], 0, 2 * segLength);
					Utilities.discreteFourierTrans(r[0], r[1], r[2], r[3]);
					
					//avg value
					double avg = r[2][0];
					toWrite[d][0][i] = avg;
					//str = str + " " + (++count) + ":" + avg;
					
					double energy = Utilities.energy(r[2]);
					toWrite[d][1][i] = energy;
					//str = str + " " + (++count) + ":" + energy;
					
					double entropy = Utilities.entropy(r[2]);
					toWrite[d][2][i] = entropy;
					//str = str + " " + (++count) + ":" + entropy;
					
					double standardDeri = Utilities.standardDeviation(r[0]);
					toWrite[d][3][i] = standardDeri;
					//str = str + " " + (++count) + ":" + standardDeri;
					
					//saveR[d] = r;
				}
				for(int d = 0; d < DataFiles.DIMENSION; ++d) {
					double[][] r = new double[2][2 * segLength];
					Utilities.twoDimensionCopy(data, beginPos, d, r[0], 0, 2 * segLength);
					Utilities.twoDimensionCopy(data, beginPos, (d + 1) % DataFiles.DIMENSION, r[1], 0, 2 * segLength);
					
					double avgAbsCrossProduct = Utilities.avgAbsCrossProduct(r[0], r[1]);
					double avg0 = Utilities.average(r[0]);
					double avg1 = Utilities.average(r[1]);
					double aacp0 = Utilities.avgAbsCrossProduct(r[0], r[0]);
					double aacp1 = Utilities.avgAbsCrossProduct(r[1], r[1]);
					
					double check0 = aacp0 - avg0 * avg0;
					double check1 = aacp1 - avg1 * avg1;
					
					double linearCor = 0;
					
					if(check0 <= NEAR_ZERO || check1 <= NEAR_ZERO) {
						linearCor = 0;
					}
					else{
						linearCor = (avgAbsCrossProduct - avg0 * avg1) / (Math.sqrt(check1) * Math.sqrt(check1));
					}
					toWrite[d][4][i] = linearCor;
					//str = str + " " + (++count) + ":" + (linearCor * LINEAR_COR_PARA);
				}
			}
			
			for(int d = 0; d < DataFiles.DIMENSION; ++d) {
				for(int j = 0; j < toWrite[d].length; ++j) {
					if(j == 4)
						toWrite[d][j] = Utilities.rescale(toWrite[d][j], SCALE_LOWER, SCALE_UPPER);
					for(int k = 0; k < toWrite[d][j].length; ++k) {
						str = str + " " + (++count) + ":" + toWrite[d][j][k];
					}
				}
			}
			file.writeln(str);
		}
		file.close();
		return fileName;
	}
	
	
	@Override 
	public String toString() {
		return "FDSVM";
	}
}
