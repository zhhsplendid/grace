package methods;

import dtwlib.DTWLib;

public class QuantizedGDTWKernel extends QuantizedDTWKernel {
	
	double GARMMA;
	
	public QuantizedGDTWKernel() {
		setGarmma(0.5);
		TRAIN_FILE = GARMMA + "_quan_gdtw_train.gsvm";
		TEST_FILE = GARMMA + "_quan_gdtw_test.gsvm";
		OUTPUT_FILE = GARMMA + "_quan_gdtw_out.gsvm";
	}
	
	public QuantizedGDTWKernel(double garmma) {
		setGarmma(garmma);
		TRAIN_FILE = GARMMA + "_quan_gdtw_train.gsvm";
		TEST_FILE = GARMMA + "_quan_gdtw_test.gsvm";
		OUTPUT_FILE = GARMMA + "_quan_gdtw_out.gsvm";
	}
	
	public double getGarmma() {
		return GARMMA;
	}
	
	public void setGarmma(double garmma) {
		GARMMA = garmma;
	}
	
	@Override
	double kernel(double data1[][], double data2[][]) {
		return Math.exp( - GARMMA * DTWLib.DTWdistance(data1, data2));
	}
	
	@Override
	public String toString() {
		return "QuantizedGDTWKernel_" + GARMMA;
	}
}
