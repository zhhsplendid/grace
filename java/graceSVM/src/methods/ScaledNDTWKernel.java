package methods;

import dtwlib.DTWLib;
import fileParser.DataFiles;
import utilities.Utilities;

public class ScaledNDTWKernel extends NDTWKernel {
	
	public final int QUAN_WIN_SIZE = 8;
    public final int QUAN_MOV_STEP = 4;
	
    public final double scaleLower = -1;
    public final double scaleUpper = 1;
    
	public ScaledNDTWKernel() {
		TRAIN_FILE = "scaled_ndtw_train.gsvm";
		TEST_FILE = "scaled_ndtw_test.gsvm";
		OUTPUT_FILE = "scaled_ndtw_out.gsvm";
	}
	
	@Override
	double[][][] readDataFromFile(int[] indices) {
		int len = indices.length;
		double[][][] data = new double[len][][];
		for(int count = 0; count < len; ++count) {
			int i = indices[count];
			double[][] rawData = DataFiles.readData(i);
			double[][] quanData = DTWLib.winStepQuantize(rawData, QUAN_WIN_SIZE, QUAN_MOV_STEP);
			data[count] = Utilities.rescale(quanData, scaleLower, scaleUpper);
		}
		return data;
	}
	
	@Override
	public String toString() {
		return "ScaledNDTW";
	}
}
