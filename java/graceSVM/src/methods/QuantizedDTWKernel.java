package methods;

import dtwlib.DTWLib;
import fileParser.DataFiles;

public class QuantizedDTWKernel extends DTWKernel {
	
	
	public final int QUAN_WIN_SIZE = 8;
    public final int QUAN_MOV_STEP = 4;
	
	public QuantizedDTWKernel() {
		TRAIN_FILE = "quan_dtw_train.gsvm";
		TEST_FILE = "quan_dtw_test.gsvm";
		OUTPUT_FILE = "quan_dtw_out.gsvm";
	}
	
	@Override
	double[][][] readDataFromFile(int[] indices) {
		int len = indices.length;
		double[][][] data = new double[len][][];
		for(int count = 0; count < len; ++count) {
			int i = indices[count];
			data[count] = DTWLib.paperQuantizeAcc(DataFiles.readData(i), QUAN_WIN_SIZE, QUAN_MOV_STEP);
		}
		return data;
	}
	
	@Override
	public String toString() {
		return "QuantizedDTWKernel";
	}
}
