package utilities;

public class Debug {
	
	public static boolean DEBUG_OUTPUT = false;
	
	
	public static void check(boolean condition) {
		check(condition, null);
	}
	
	public static void check(boolean condition, String str) {
		if(!condition) {
			if(str != null){
				System.err.println(str);
			}
			else {
				System.err.println("check err");
			}
			System.exit(1);
		}
	}
	
	public static void print(String str) {
		if(DEBUG_OUTPUT) {
			System.out.print(str);
		}
	}
	
	public static void println(String str) {
		if(DEBUG_OUTPUT) {
			System.out.println(str);
		}
	}
	
}
