package utilities;

public class Utilities {
	
	public static boolean contains(int[] arr, int element) {
		for(int i = 0; i < arr.length; ++i) {
			if(arr[i] == element) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean equalPrefix(double[][] arr1, double[][] arr2, int len) {
		for(int i = 0; i < len; ++i) {
			if(arr1[i].length != arr2[i].length) {
				return false;
			}
			
			for(int j = 0; j < arr1[i].length; ++j) {
				if(arr1[i][j] != arr2[i][j]) {
					return false;
				}
			}
		}
		return true;
	}
	
	public static boolean equalPrefix(int[] arr1, int[] arr2, int len) {
		for(int i = 0; i < len; ++i) {
			if(arr1[i] != arr2[i]) {
				return false;
			}
		}
		return true;
	}
	
	public static double max(double[] x) {
		double max = x[0];
		for(int i = 1; i < x.length; ++i) {
			if(x[i] > max) {
				max = x[i];
			}
		}
		return max;
	}
	
	public static double max(double[][] x) {
		double max = max(x[0]);
		for(int i = 1; i < x.length; ++i) {
			double tmp = max(x[i]);
			if(tmp > max) {
				max = tmp;
			}
		}
		return max;
	}
	
	public static double min(double[] x) {
		double min = x[0];
		for(int i = 1; i < x.length; ++i) {
			if(x[i] < min) {
				min = x[i];
			}
		}
		return min;
	}
	
	public static double min(double[][] x) {
		double min = min(x[0]);
		for(int i = 1; i < x.length; ++i) {
			double tmp = min(x[i]);
			if(tmp < min) {
				min = tmp;
			}
		}
		return min;
	}
	
	public static double[][] rescale(double[][] data, double lower, double upper) {
		double max = max(data);
		double min = min(data);
		if(max == min) {
			return data;
		}
		double k = (upper - lower) / (max - min);
		double[][] ret = new double[data.length][];
		for(int i = 0; i < data.length; ++i) {
			ret[i] = new double[data[i].length];
			for(int j = 0; j < data[i].length; ++j) {
				ret[i][j] = (data[i][j] - min) * k + lower;
			}
		}
		return ret;
	}
	
	public static double[] rescale(double[] data, double lower, double upper) {
		double max = max(data);
		double min = min(data);
		if(max == min) {
			return data;
		}
		double k = (upper - lower) / (max - min);
		double[] ret = new double[data.length];
		for(int i = 0; i < data.length; ++i) {
			ret[i] = (data[i] - min) * k + lower;
		}
		return ret;
	}
	
	public static void printArray(int[] arr) {
		for(int i = 0; i < arr.length; ++i) {
			System.out.print(arr[i] + " ");
		}
	}
	
	public static void printArray(double[] arr) {
		for(int i = 0; i < arr.length; ++i) {
			System.out.print(arr[i] + " ");
		}
	}
	
	public static void printlnArray(int[] arr) {
		printArray(arr);
		System.out.println();
	}
	
	public static void printlnArray(double[] arr) {
		printArray(arr);
		System.out.println();
	}
	
	public static int[] continuousArray(int end) {
		return continuousArray(0, end, 1);
	}
	
	public static int[] continuousArray(int begin, int end) {
		return continuousArray(begin, end, 1);
	}
	
	public static int[] continuousArray(int begin, int end, int step) {
		int[] ret = new int[(end - begin) / step + 1];
		for(int i = 0; i < ret.length; ++i) {
			ret[i] = begin + i * step;
		}
		return ret;
	}
	
	/**
	 * this function is copied from website
	 * @param inreal
	 * @param inimag
	 * @param outreal
	 * @param outimag
	 */
	public static void discreteFourierTrans(double[] inreal, double[] inimag, double[] outreal, double[] outimag) {
	    int n = inreal.length;
	    for (int k = 0; k < n; k++) {  // For each output element
	        double sumreal = 0;
	        double sumimag = 0;
	        for (int t = 0; t < n; t++) {  // For each input element
	            double angle = 2 * Math.PI * t * k / n;
	            sumreal +=  inreal[t] * Math.cos(angle) + inimag[t] * Math.sin(angle);
	            sumimag += -inreal[t] * Math.sin(angle) + inimag[t] * Math.cos(angle);
	        }
	        outreal[k] = sumreal;
	        outimag[k] = sumimag;
	    }
	}
	
	public static void twoDimensionCopy(double[][] src, int begin, int dimen, double[] dest, int dbegin, int len) {
		for(int i = 0; i < len; ++i) {
			dest[dbegin + i] = src[begin + i][dimen];
		}
	}
	
	public static double energy(double[] x) {
		int len = x.length;
		double answer = 0;
		for(int i = 0; i < len; ++i) {
			answer += (x[i] * x[i]);
		}
		return answer / len;
	}
	
	public static double sum(double[] x) {
		double answer = 0;
		for(int i = 0; i < x.length; ++i) {
			answer += x[i];
		}
		return answer;
	}
	
	public static double average(double[] x) {
		return sum(x) / x.length;
	}
	
	public static double absSum(double[] x) {
		double answer = 0;
		for(int i = 0; i < x.length; ++i) {
			answer += Math.abs(x[i]);
		}
		return answer;
	}
	
	public static double entropy(double[] x) {
		int len = x.length;
		double sum = absSum(x);
		if(sum == 0) {
			return 0;
		}
		double answer = 0;
		
		for(int i = 0; i < len; ++i) {
			double prob = Math.abs(x[i]) / sum;
			if(prob == 0) {
				continue;
			}
			answer += (prob * Math.log(1 / prob));
		}
		return answer;
	}
	
	public static double standardDeviation(double[] x) {
		double answer = 0;
		double avg = average(x);
		for(int i = 0; i < x.length; ++i) {
			answer += ((x[i] - avg) * (x[i] - avg));
		}
		answer = Math.sqrt(answer);
		return answer;
	}
	
	public static double avgAbsCrossProduct(double[] x, double y[]) {
		Debug.check(x.length == y.length);
		double sum = 0;
		for(int i = 0; i < x.length; ++i) {
			sum += (Math.abs(x[i] * y[i])); 
		}
		return sum / x.length;
	}
}
