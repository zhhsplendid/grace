package main;

import methods.*;
import experiments.Experiment;
import fileParser.DataFiles;
import fileParser.FileParser;

public class Main {
	
	public static final String DATA_DIR = "data/";
	public static final String TMP_DATA_DIR = "tmp/";
	public static final String EXP_OUT_DIR = "experiments_results/";
	
	public static ApproachMethod[] methods = {
			//new KNearestDTW(),
			//new UWave(), 
			//new KNearestDTW(2),
			//new KNearestDTW(3),
			//new KNearestDTW(4),

			//new NaiveSegmentation(), 
			//new FDSVM(),
			new QuantizedNDTWKernel(), 
			//new NDTWKernel(), 
			//new QuantizedGDTWKernel(), 
			//new GDTWKernel(),
			//new ScaledNDTWKernel(), 
			//new ScaledGDTWKernel(),
			};
	
	
	public static void main(String[] args) {
		//ithTest();
		for(int i = 4; i <= 4; ++i) {
			//everyUserExperiment(i);
			
			//everyUserEveryDayExperiment(i);
			crossUser(i);
		}
		
	}
	
	public static void ithTest() {
		for(int i = 0; i < methods.length; ++i) {
			ApproachMethod aMethod = methods[i];
			Experiment experiment = new Experiment(aMethod);
			FileParser file = new FileParser(EXP_OUT_DIR, 
					"ith_" + aMethod.toString() + ".gsvm");
			double avgAccuracy = 0;
			
			int testTimes = DataFiles.TOTAL_DAYS * DataFiles.TOTAL_REPEAT;
			for(int user = 1; user <= DataFiles.TOTAL_USERS; ++user) {
				for(int t = 0; t < testTimes; ++t){
					System.out.println("ith_" + aMethod.toString() + " " + testTimes + " "+ t + 
						" " + DataFiles.TOTAL_USERS + " " + user);
				
					experiment.setIthTest(user, t);;
					double accuracy = experiment.run();
					file.writeln(user + " " + " " + t + " " + accuracy);
					avgAccuracy += accuracy;
				}
			}
			avgAccuracy = avgAccuracy / DataFiles.TOTAL_USERS / testTimes;
			file.writeln(Double.toString(avgAccuracy));
			file.close();
		}
	}
	
	public static void everyUserExperiment(int templateNum) {
		for(int i = 0; i < methods.length; ++i) {
			ApproachMethod aMethod = methods[i];
			Experiment experiment = new Experiment(aMethod);
			FileParser file = new FileParser(EXP_OUT_DIR, 
					"eu_" + templateNum + "_" + aMethod.toString() + ".gsvm");
			double avgAccuracy = 0;
			
			for(int user = 1; user <= DataFiles.TOTAL_USERS; ++user) {
				
				System.out.println("eu" + templateNum + "_" + aMethod.toString() + 
						" " + DataFiles.TOTAL_USERS + " " + user);
				
				experiment.setEveryUserData(user, templateNum);
				double accuracy = experiment.run();
				file.writeln(user + " " + accuracy);
				avgAccuracy += accuracy;
			}
			avgAccuracy = avgAccuracy / DataFiles.TOTAL_USERS;
			file.writeln(Double.toString(avgAccuracy));
			file.close();
		}
	}
	
	public static void everyUserEveryDayExperiment(int templateNum) {
		for(int i = 0; i < methods.length; ++i) {
			ApproachMethod aMethod = methods[i];
			Experiment experiment = new Experiment(aMethod);
			FileParser file = new FileParser(EXP_OUT_DIR, 
					"eued_" + templateNum + "_" + aMethod.toString() + ".gsvm");
			double avgAccuracy = 0;
			
			for(int user = 1; user <= DataFiles.TOTAL_USERS; ++user) {
				for(int day = 1; day <= DataFiles.TOTAL_DAYS; ++day){
					
					System.out.println("eued_" + templateNum + "_" + aMethod.toString() + 
							" " + DataFiles.TOTAL_USERS + " " + user +
							DataFiles.TOTAL_DAYS + " " + day);
					
					experiment.setEveryUserEveryDayData(user, day, templateNum);
					double accuracy = experiment.run();
					file.writeln(user + " " + day + " " + accuracy);
					avgAccuracy += accuracy;
				}
			}
			avgAccuracy = avgAccuracy / (DataFiles.TOTAL_USERS * DataFiles.TOTAL_DAYS);
			file.writeln(Double.toString(avgAccuracy));
			file.close();
		}
	}
	
	public static void fractionExpr(double frac) {
		for(int i = 0; i < methods.length; ++i) {
			ApproachMethod aMethod = methods[i];
			Experiment experiment = new Experiment(aMethod);
			FileParser file = new FileParser(EXP_OUT_DIR, 
					"fraction_" + frac + "_" + aMethod.toString() + ".gsvm");
			
			experiment.setDataByFraction(frac);
			
			System.out.println("fraction_" + frac + "_" + aMethod.toString() + " " + frac);
			double accuracy = experiment.run();
			file.writeln(Double.toString(accuracy));
			file.close();
		}
	}
	
	public static void crossUser(int numTemplate) {
		for(int i = 0; i < methods.length; ++i) {
			ApproachMethod aMethod = methods[i];
			Experiment experiment = new Experiment(aMethod);
			FileParser file = new FileParser(EXP_OUT_DIR, "crossUser_" + numTemplate + "_"
					+ aMethod.toString() + ".gsvm");
			
			double avgAccuracy = 0;
			
			for(int user = 1; user <= DataFiles.TOTAL_USERS; ++user) {
				
					
					System.out.println("crossUser_" + numTemplate + "_" + aMethod.toString() + 
							" " + DataFiles.TOTAL_USERS + " " + user);
					
				experiment.setCrossUserData(numTemplate, user);
				double accuracy = experiment.run();
				file.writeln(user + " " + accuracy);
				avgAccuracy += accuracy;
			}
			avgAccuracy = avgAccuracy / (DataFiles.TOTAL_USERS);
			file.writeln(Double.toString(avgAccuracy));
			file.close();
		}
	}
}
