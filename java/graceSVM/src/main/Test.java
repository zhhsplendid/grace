package main;

import dtwlib.DTWLib;
import fileParser.DataFiles;
import utilities.Utilities;

public class Test {
	
	public static boolean testInteresting(int num){
		
		for(int i = 0; i < num; ++i) {
			double[][] data = DataFiles.readData(i);
		
			double[][] q1 = DTWLib.interestingQuantize(
					data, DTWLib.QUAN_WIN_SIZE, DTWLib.QUAN_MOV_STEP);
			int len = DTWLib.quantizeAcc(data, data.length);
			if(!Utilities.equalPrefix(q1, data, len)) {
				return false;
			}
		}
		
		return true;
	}
}
