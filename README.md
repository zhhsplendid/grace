# wireless_network_project
Wireless course CS386 research project

Huihuang Zheng, Splendid Zheng, Wayne Zheng (郑辉煌)
Yiming Pang (庞一明)

Conclusion:
	Nice work! Almost finish! Next step may be published?

Usage:

(1) Android App:
	Using Android Studio to write our project in ./android/ into your android phone so you can run it. The ./texDoc/Final_Report/Final_Report.pdf shows what this is for.

(2) Experiments of improving world best results:
	Modify and run ./java/graceSVM/src/main/Main.java you can choose what experiment to run at the head of the file