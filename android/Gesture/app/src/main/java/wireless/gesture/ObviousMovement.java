package wireless.gesture;

/**
 * Created by zhhsp on 11/20/2015.
 */
public class ObviousMovement {

    public static final int COUNT_THRESHOLD = 5;
    public static final double LARGE_MOVEMENT = 1;

    protected int count;
    public boolean moving;

    public ObviousMovement() {
        count = 0;
        moving = false;
    }

    public boolean isObvious(double accelerations[]) {
        if(moving == false) {
            if(norm(accelerations) > LARGE_MOVEMENT) {
                ++count;
                if(count > COUNT_THRESHOLD) {
                    count = 0;
                    moving = true;
                    return true;
                }
            }
            else {
                count = 0;
            }
        }
        else { //moving == true
            if(norm(accelerations) < LARGE_MOVEMENT) {
                ++count;
                if(count > COUNT_THRESHOLD) {
                    count = 0;
                    moving = false;
                    return true;
                }
            }
            else {
                count = 0;
            }
        }
        return false;
    }

    public double norm(double accelerations[]) {
        double ret = 0;
        for(int i = 0; i < accelerations.length; ++i) {
            ret += (accelerations[i] * accelerations[i]);
        }
        ret = Math.sqrt(ret);
        return ret;
    }
}
