package wireless.gesture;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.*;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.Set;

public class MainActivity extends AppCompatActivity implements SensorEventListener{
    // For output show acceleration of x,y,z axises.
    protected TextView xAcceleration;
    protected TextView yAcceleration;
    protected TextView zAcceleration;

    // For first version, I just use number as output, finally  we will use pictures and add actions
    // of those recognized gestures.
    protected TextView textResult;

    protected SensorManager sensorManager;
    protected Sensor accelerateSensor;
    protected DTWLib dtwLib;

    final int DIMENSION = DTWLib.DIMENSION;
    protected double gravity[] = new double[DIMENSION];
    protected double linear_acceleration[] = new double[DIMENSION];
    protected boolean recording = false;
    protected boolean detecting = false;
    protected boolean slideMode = false;
    protected boolean touchMode = false;

    Button recordButton;
    Button recognizeButton;
    Button endButton;
    Button clearButton;
    Button slideButton;
    Button controlWhenPushButton;

    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    protected BluetoothCommandService mCommandService = null;
    Set<BluetoothDevice> bondedDevices ;
    ObviousMovement moveMeasure;

    final double alpha = 0.8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerateSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(this, accelerateSensor, SensorManager.SENSOR_DELAY_NORMAL);

        dtwLib = new DTWLib(this);

        moveMeasure = new ObviousMovement();

        xAcceleration = (TextView) findViewById(R.id.xAcceleration);
        yAcceleration = (TextView) findViewById(R.id.yAcceleration);
        zAcceleration = (TextView) findViewById(R.id.zAcceleration);
        textResult = (TextView) findViewById(R.id.textResult);

        recordButton = (Button) findViewById(R.id.recordButton);
        recordButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                textResult.setText("Recording");
                if(!detecting && !recording) {
                    recording = true;
                    slideMode = false;
                    touchMode = false;
                    dtwLib.recordMode();
                    dtwLib.beginGesture();
                }
            }
        });

        recognizeButton = (Button) findViewById(R.id.recognizeButton);
        recognizeButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                if(!recording && !detecting) {
                    detecting = true;
                    slideMode = false;
                    touchMode = false;
                    textResult.setText("Detecting");
                    dtwLib.detectMode();
                    dtwLib.beginGesture();
                }
            }
        });

        endButton = (Button) findViewById(R.id.endButton);
        endButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                if(recording) {
                    recording = false;
                    int totalGestures = dtwLib.endGesture();
                    textResult.setText("record over, total " + totalGestures + " gestures");
                }
                if(detecting) {
                    detecting = false;
                    int intResult = dtwLib.endGesture();
                    double minDist = dtwLib.minDist;
                    textResult.setText("result = " + Integer.toString(intResult) + '\n' + "minDist = " + minDist);
                }
                if(slideMode) {
                    slideMode = false;
                    textResult.setText("end slide mode");
                }
            }
        });

        clearButton = (Button) findViewById(R.id.clearButton);
        clearButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                int deletedGestures = dtwLib.clearGestures();
                textResult.setText("result = clear " + deletedGestures + " gestures");
            }
        });

        slideButton = (Button) findViewById(R.id.slideButton);
        slideButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                recording = false;
                detecting = false;
                slideMode = true;
                touchMode = false;
                dtwLib.detectMode();
                turnOnSlideMode();
            }
        });

        controlWhenPushButton = (Button) findViewById(R.id.controlButton);
        controlWhenPushButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                touchMode = false;
                int intResult = dtwLib.endGesture();
                //double minDist = dtwLib.minDist;
                sendCommand(intResult);
            }
        });
        controlWhenPushButton.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                recording = false;
                detecting = false;
                slideMode = false;
                touchMode = true;
                turnOnSlideMode();
                dtwLib.detectMode();
                //dtwLib.beginGesture();
                return false;
            }
        });
    }

    protected void turnOnSlideMode() {
        textResult.setText("slide mode");

        if(mBluetoothAdapter == null) {
            slideMode = false;
            textResult.setText("Device doesn't support blue tooth.");
            return;
        }
        //slideMode = true;

        if (mCommandService == null) {
            mCommandService = new BluetoothCommandService(this);
        }
        bondedDevices = mBluetoothAdapter.getBondedDevices();
        BluetoothDevice device = null;
        int chooseDeviceIndex = 0; //hand code choosing device
        int count = 0;
        for(BluetoothDevice tmpDevice: bondedDevices) {
            if(count == chooseDeviceIndex) {
                device = tmpDevice;
                break;
            }
            ++count;
        }
        if(device == null) {
            slideMode = false;
            textResult.setText("No bonded device");
            return;
        }
        mCommandService.connect(device);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mCommandService != null) {
            mCommandService.stop();
        }
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    public void onSensorChanged(SensorEvent event) {
        /*
        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];

        linear_acceleration[0] = event.values[0] - gravity[0];
        linear_acceleration[1] = event.values[1] - gravity[1];
        linear_acceleration[2] = event.values[2] - gravity[2];
        */
        if (recording || detecting || touchMode) {
            linear_acceleration[0] = event.values[0]; // DTWLib.EARTH_GRAVITY;
            linear_acceleration[1] = event.values[1]; // DTWLib.EARTH_GRAVITY;
            linear_acceleration[2] = event.values[2] - DTWLib.EARTH_GRAVITY; // DTWLib.EARTH_GRAVITY - 1;
            xAcceleration.setText("x = " + Double.toString(linear_acceleration[0]));
            yAcceleration.setText("y = " + Double.toString(linear_acceleration[1]));
            zAcceleration.setText("z = " + Double.toString(linear_acceleration[2]));
            dtwLib.addAccerelation(linear_acceleration);
        } else if (slideMode) {
            //mCommandService.write(BluetoothCommandService.KEY_LEFT); //for debug test

            linear_acceleration[0] = event.values[0]; // DTWLib.EARTH_GRAVITY;
            linear_acceleration[1] = event.values[1]; // DTWLib.EARTH_GRAVITY;
            linear_acceleration[2] = event.values[2] - DTWLib.EARTH_GRAVITY; // DTWLib.EARTH_GRAVITY - 1;
            xAcceleration.setText("x = " + Double.toString(linear_acceleration[0]));
            yAcceleration.setText("y = " + Double.toString(linear_acceleration[1]));
            zAcceleration.setText("z = " + Double.toString(linear_acceleration[2]));

            //textResult.setText("moving? " + moveMeasure.moving);

            if (moveMeasure.moving) {
                dtwLib.addAccerelation(linear_acceleration);
                if (moveMeasure.isObvious(linear_acceleration)) { // change to no moving
                    int intResult = dtwLib.endGesture();
                    sendCommand(intResult);
                }
            } else {// not moving
                if (moveMeasure.isObvious(linear_acceleration)) {
                    textResult.setText("begin moving");
                    dtwLib.beginGesture();
                    dtwLib.addAccerelation(linear_acceleration);
                }
            }
        }
    }

    protected void sendCommand(int intResult) {
        switch (intResult) {
            case BluetoothCommandService.KEY_LEFT:
                textResult.setText("sent command left key");
                mCommandService.write(BluetoothCommandService.KEY_LEFT);
                break;
            case BluetoothCommandService.KEY_RIGHT:
                textResult.setText("sent command right key");
                mCommandService.write(BluetoothCommandService.KEY_RIGHT);
                break;
            case BluetoothCommandService.KEY_F5:
                textResult.setText("sent command F5");
                mCommandService.write(BluetoothCommandService.KEY_F5);
                break;
            default:
                textResult.setText("Don't know command " + intResult);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dtwLib.close();
        sensorManager.unregisterListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        dtwLib.open();
        sensorManager.registerListener(this, accelerateSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
