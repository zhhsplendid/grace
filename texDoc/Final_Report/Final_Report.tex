
% ---------------------------------------------------------------------------------------------------------------
% It is an example which *does* use the .bib file (from which the .bbl file
% is produced).
% REMEMBER HOWEVER: After having produced the .bbl file,
% and prior to final submission,
% you need to 'insert'  your .bbl file into your source .tex file so as to provide
% ONE 'self-contained' source file.


\documentclass{acm_proc_article-sp}

\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{subfigure}
\usepackage{graphicx}
\usepackage{url}
\usepackage{amsfonts}
\usepackage{amssymb}

\newtheorem{theorem}{Theorem}
\newdef{definition}{Definition}

\begin{document}

\title{GRACE: Gesture Recognition via 3D Accelerometer}


%
% You need the command \numberofauthors to handle the 'placement
% and alignment' of the authors beneath the title.
%
% For aesthetic reasons, we recommend 'three authors at a time'
% i.e. three 'name/affiliation blocks' be placed beneath the title.
%
% NOTE: You are NOT restricted in how many 'rows' of
% "name/affiliations" may appear. We just ask that you restrict
% the number of 'columns' to three.
%
% Because of the available 'opening page real-estate'
% we ask you to refrain from putting more than six authors
% (two rows with three columns) beneath the article title.
% More than six makes the first-page appear very cluttered indeed.
%
% Use the \alignauthor commands to handle the names
% and affiliations for an 'aesthetic maximum' of six authors.
% Add names, affiliations, addresses for
% the seventh etc. author(s) as the argument for the
% \additionalauthors command.
% These 'additional authors' will be output/set for you
% without further effort on your part as the last section in
% the body of your article BEFORE References or any Appendices.

\numberofauthors{3} %  in this sample file, there are a *total*
% of EIGHT authors. SIX appear on the 'first-page' (for formatting
% reasons) and the remaining two appear in the \additionalauthors section.
%
\author{
% You can go ahead and credit any number of authors here,
% e.g. one 'row of three' or two rows (consisting of one row of three
% and a second row of one, two or three).
%
% The command \alignauthor (no curly braces needed) should
% precede each author name, affiliation/snail-mail address and
% e-mail address. Additionally, tag each line of
% affiliation/address with \affaddr, and tag the
% e-mail address with \email.
%
% 1st. author
\alignauthor
Huihuang Zheng\\
       \affaddr{University of Texas at Austin}\\
       \email{huihuang@utexas.com}
% 2nd. author
\alignauthor
Yiming Pang\\
       \affaddr{University of Texas at Austin}\\
       \email{ypang@cs.utexas.edu}
}


\maketitle
\begin{abstract}
The rapid growth of various sensors on smart devices provides us with new opportunities to leverage the abundant sensor data on smart devices to diversify user interaction based on gestures. Among all of these sensors, 3D accelerometer provides accurate data of the movement of the smart device which can be fully exploited to describe different gestures. In this paper, we present GRACE: \underline{G}esture \underline{R}ecognition vi\underline{A} 3D a\underline{C}c\underline{E}lerometer. The idea of GRACE mainly comes from the previous work of uWave~\cite{liu2009uwave}. In GRACE, we implement different algorithms for gesture recognition mainly based on dynamic time warping (DTW) and suppport vector machine (SVM). With the evaluation results, we can show that we improve the accuracy of gesture recognition compared to currently existing systems. What is more, we also migrate GRACE to Android platform and developed an application, which can be furtherly developed for multiple use.
\end{abstract}


\category{Wireless Network}{Research Project}[Report]

\terms{Algorithm, Application}

\keywords{Gesture Recognition, Accelerometer, Machine Learning} % NOT required for Proceedings

\section{Introduction}
\label{introduction}
Gesture-based interactions are now widely adopted in all kinds of devices such as smartphone, smart watch, smart glasses and GamePads. In general, gesture-based interaction usually requires pattern recognition. Several techniques have been proposed for gesture recognition and among all of them there are mainly two categories: vision-based and device-based. Vision-based gesture recognition has attracted a lot of attention from the industry these years and there are already some off the shelf equipments available. For example, Xbox kinect of Microsoft enables users to control and interact with their console/computer without the need for a game controller, through a natural user interface using gestures and spoken commands. Another category of gesture recognition is mainly based on wearable devices such as ``smart gloves'' which can detect the motion of hands and convert the motion data to specified commands. However, these two approaches usually have a high requirement against computational capability and power control and often require some special devices, which reduces the availability in daily life. In our project, we mainly focus on implementing a light-weighted gesture recognition system based on 3D accelerometer, which is commonly equipped on smart phones. We also migrate the gesture recognition system from Wii remote (as in the paper of uWave) to Android phone which is more widely available.

Our system GRACE is mainly inspired by the framework of previous work uWave~\cite{liu2009uwave}, which is an accelerometer-based personalized gesture recognition system. uWave requires a large gesture library acting as the templates. When uWave is initiated and a gesture is detected, it quantizes the acceleration data and uses DTW to compute time series Euclidean distances between the detected gesture data and predefined templates. Finally it outputs the template which matches the best as the recognition result. Therefore, uWave is a light-weighted system since it doesn't require high computational power or heavy training compared to the statistical methods.

Besides DTW, we also study the method of SVM, which is also a powerful tool in machine learning area. We try to combine DTW and SVM to provide better result for gesture recognition. In GRACE, we implement a method of SVM with NDTW/GDTW kernel which performs the best.

Our contributions are summarized as follows:
\begin{itemize}
	\item We implemente 9 different algorithms for gesture recognition and compare their performance by conducting extensive evaluations for each algorithm.
	\item We develope an application on Android platform, which enables us to do gesture recognition on regular smart phones. We also implemente a slide control function by hand gesture for the laptop based on bluetooth communication.
	\item To the best of our knowledge, GRACE is the first gesture recognition system which incorporates SVM and DTW simultaneously. The evaluation results show that the performance of GRACE is better than current systems.
\end{itemize}

The remainder of this report is organized as follows: In Section~\ref{relatedworks}, we discuss some related works of gesture recognition. In Section~\ref{preliminaries}, we introduce some necessary preliminaries. In Section~\ref{systemmodel} we present the detailed designs of GRACE and in section~\ref{application} we introduce the application on Android platform followed by the evaluation results in Section~\ref{evaluation}. Finally, we conclude our work in Section~\ref{conclusion}.

\section{Related Works}
\label{relatedworks}
uWave~\cite{liu2009uwave} is an efficient gesture recognition method based on a single accelerometer using DTW. The basic idea of uWave is shown in Figure~\ref{uWaveBasic}. When uWave is active, it takes continuous acceleration data as input to a quantization module to convert the accelerometer data to discrete values thus reducing floating point computation. The quantized data is presented as three vectors, corresponding to the accelerations along the three axes. Then it matches the quantized data with data in a template library using DTW, note that the data in template library is also quantized. uWave matches the gesture with the template that minimizes the Euclidean distance between the input gesture and template. After the matching, the recognition result can be furtherly used to adapt the existing templates to accommodate gesture variations over time.
\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{uWave_basic.JPG}
	\caption{Basic idea of uWave}
	\label{uWaveBasic}
\end{figure}

Similar to uWave, in~\cite{akl2010accelerometer}, Akl and Valaee also introduce a accelerometer-based gesture recognition. Besides DTW, they also incorporate affinity propagation algorithm  for training and
utilize the sparse nature of the gesture sequence by implementing compressive sensing for gesture recognition. By leveraging the sparse nature of the gesture sequence, it can avoid jumping into large amount of training data. Also, it works well for both user-dependent recognition and user-independent recognition.

Another work by Mace et al.~\cite{mace2013naive} compares the performance of DTW and na\"{i}ve bayesian classifiers. Compared with DTW, na\"{i}ve Bayesian classification is also a promising technique in gesture recognition because it can make accurate predictions by using statistical measures to calculate membership probabilities with low computation overheads.

In~\cite{wu2009gesture}, the authors present FDSVM (Frame-based Descriptor and multi-class SVM) for gesture recognition. The acceleration data of a gesture is collected and represented by a frame-based descriptor, to extract the discriminative information. Then a SVM-based multi-class gesture classifier is built for recognition in the nonlinear gesture feature space. FDSVM introduced the concept of frame in gesture recognition. To describe the whole gesture but distinguish the periods from each other, FDSVM divides a gesture into $N+1$ segments identical in length, and then every two adjunct segments make up a frame. For each frame, it calculates 5 features as the input data of SVM, namely mean, energy, entropy, standard deviation and correlation. Among these 5 features, mean, energy and entropy are in the frequencey domain, which can be calculated by a discrete Fourier transform.
\section{Preliminaries}
\label{preliminaries}
In this section, we will present the details about DTW and SVM, which are the 2 most important tools employed in the design of GRACE.

\subsection{Dynamic Time Warping}
\begin{figure}
		\subfigure[Graphic illustration of the recursive algorithm]{
			\label{2dplane}
			\includegraphics[scale=0.5]{2dplane.png}
		}
		\subfigure[Algorithm for computing the DTW distance]{
			\label{dtwalgo}
			\includegraphics[scale=0.4]{dp.png}
		}
		\caption{Dynamic Time Warping (DTW) algorithm}
		\label{dtw}
	\vspace{-5mm}
\end{figure}
Dynamic time warping (DTW) is a classical algorithm based on dynamic programming to match two time series with temporal dynamics~\cite{myers1981DTW}, given the function for calculating the distance between two time samples. In our design, we employ the Euclidean distance for matching quantized time series of acceleration. We borrow the figure from~\cite{liu2009uwave} to illustrate the matching process. Suppose that we have two time series $S$ and $T$ denoted as $S[1,\dots,M]$ and $T[1,\dots,N]$, we can regard the matching between $S[1,\cdots,M]$ and $T[1,\dots,N]$ as a path from $(1,1)$ to $(M,N)$ on a 2-dimension grid as shown in Figure~\ref{2dplane}, . Each point along the path, say $(i,j)$, indicates that $S[i]$ is matched with $T[j]$. Our goal is to find a path that minimizes the accumulative distance between $S$ and $T$.

Figure~\ref{dtwalgo} illustrates how the algorithm of DTW works. DTW employs dynamic programming to calculate the distance and find the corresponding optimal path. The optimal path from $(0,0)$ to $(i,j)$ can be possibly obtained from three predecessors of $(i,j)$, namely $(i-1,j)$, $(i,j-1)$ and $(i-1,j-1)$. The distance from $(0,0)$ to $(i,j)$ is therefore the distance at $(i,j)$ plus the smallest distance of the three predecessor candidates.
\subsection{Support Vector Machine}
Support Vector Machines (SVM) are powerful tools in machine learning area. SVM is supervised learning model with associated learning algorithms that analyze data and recognize patterns, used for classification and regression analysis.~\cite{svm}. Since the detailed implementation of SVM is not the focus of our project, we will not jump into details of SVM and will just provide some high-level background knowledge of SVM.

\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{svm-plane.png}
	\caption{SVM Example}
	\label{svm-plane}
\end{figure}
In our design of GRACE, SVM is mainly employed for classification problems. For linear classification problem, SVM considers input data as a $p-$dimensional vector and the goal is to find a $p-1-$dimensional hyperplane to separate  the input data points. If there exists several hyperplanes that satisfy the requirement, one reasonable choice as the best hyperplane is the one that represents the largest separation, or margin, between the two classes. Hence we choose the hyperplane so that the distance from it to the nearest data point on each side is maximized. If such a hyperplane exists, it is known as the maximum-margin hyperplane and the linear classifier it defines is known as a maximum margin classifier. A typical example is illustrated in Figure~\ref{svm-plane}. In this scenario, we have two classes of data: black ones and white ones. They are all in a 2-dimensional space and we want to separate them with a straight line. As shown in the picture, we can see that both $H_2$ and $H_3$ satisfy the requirement of separating data. Since we are looking for the line that can maximize the margin for both data sets, $H_3$ is obviously a better choice.

Besides linear classification, SVM can also deal with highly nonlinear classification and regression problems.

Another important concept in SVM is the kernel method. Generally, kernel methods are a class of algorithms for pattern analysis. Kernel methods are implemented as kernel functions in SVM and these functions enable SVM to operate classification or regression in high-dimensional, implicit feature space efficiently. By evaluating different kernel functions in SVM training process, we can improve the result by adopting the best-fit kernel function for our application.
\section{System Design}
\label{systemmodel}
In this section, we present the details of 9 different algorithms implemented in GRACE.
\subsection{uWave}
We implement the method in~\cite{liu2009uwave} as a baseline of the system so that we can compare the performance of different algorithms.

Upon receving the acceleration data, first we have to input the data to a quantization module. Quantization can significantly reduce the computation overhead by decreasing the length of the input time series. Also we can convert the input continuous accelerations to discrete values which can reduce floating point computation. Besides reducing computational overheads, another benefit brought by quantization is accuracy. Quantization can remove variations that are not intrinsic to the gesture such as accelerometer nosie or minor hand tilt.

In our design, we set an averaging window of 8ms and move at a 4ms step, which means that we average the acceleration data within an interval of 8ms and each time move forwards by 4ms. For instance, we average the acceleration data from 0-8ms, then we average the data from 4-12ms and then 8-16ms and so forth. This method can reduce the length of the time series for DTW. The rationale behind it is that the acceleration of hand movement does not change erratically and rapid changes in acceleration are often caused by noise and minor hand tilt. Besides the averaging window, we also apply a non-linear layering as in Table~\ref{layering}
\begin{table}[!hbp]
	\centering
\begin{tabular}{|l|l|}
\hline
Acceleration Data (a) & Converted Value \\
\hline
$a>2g$ & 16\\
\hline
$g<a<2g$ & 11$\sim$15(five levels linearly)\\
\hline
$0<a<g$ & 1$\sim$10(ten levels linearly)\\
\hline
$a=0$ & 0\\
\hline
$-g<a<0$ & -1$\sim$-10(ten levels linearly)\\
\hline
$-2g<a<-g$ & -11$\sim$-15(five levels linearly)\\
\hline
$a<-2g$ & -16\\
\hline
\end{tabular}
\caption{Non-linear Layering}
\label{layering}
\end{table}

The rationale behind the non-linear layering is that by our observation, we find out that most of the accelerations are within the range of $-2g$ to $2g$. With this layering, we can furtherly normalize the data and reduce the computation overheads.

One obvious advantage of DTW is that it only requires very little training data. Therefore, we can only use a small fraction as the training data and others as testing data. In our design, we can use only one gesture data as the template and do the gesture reconition based on DTW as we introduced in Section~\ref{preliminaries}.

\subsection{SVM with Linear Kernel}

The second algorithm we implement is the most basic SVM with linear kernel. As we introduced in section~\ref{preliminaries}, SVM with linear kernel can accomplish linear classification by a hyperplane. We use the acceleration data as training features and input them to SVM thus we can get a simple classifier of different gestures. Then once we detect a gesture, we input the data to the classifier and get the result of which gesture it is classified to.

\subsection{FDSVM}

FDSVM~\cite{wu2009gesture} stands for frame-based gesture descriptor and multi-class SVM. We can denote a gesture as:
\[G = (a_x,a_y,a_z)\]
where $a_T = (a_T^0,a_T^1,\dots,a_T^{L-1}),T=x,y,z$ is the acceleration vector of an axis and $L$ is the length of the temporal sequence. To describe the whole gesture meanwhile distinguish the periods from each other, the time series is divided into $N+1$ segments with identical length and we make up every two adjacent segments as a frame as shown in Figure~\ref{fdsvm}.
\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{fdsvm.png}
	\caption{Illustration of segments and frames for a gesture}
	\label{fdsvm}
\end{figure}

Each frame $k$ can be represented as:
\[R_k = (r_{x,k},r_{y,k},r_{z,k}) \quad k = 0,1,\dots,N-1\]
\[r_{T,k} = (r_{T,k}^0,r_{T,k}^1,\dots,r_{T,k}^{2L_s-1}),\quad T = x,y,z\]
\[r_{T,k}^n = a_{T}^{kL_s+n},\quad n = 0,\dots,2L_s-1\]
where $L_s = \lfloor L/(N+1)\rfloor$ is the length of a segment. We extract five features for SVM training which will be introduced in detail.

According to signal processing theory, features in frequency domain can provide rich information about the signal. Hence, we should not only concentrate on temporal features but also take spectral features into consideration. We denote the feature-type set as $\mathcal{F}$. $\mathcal{F}$ consists of 5 main features, mean value $\mu$, energy $\epsilon$, entropy $\delta$, standard deviation $\sigma$ of the amplitude and correlation $\gamma$ among the three axes $x,y$ and $z$. Mean, energy and entropy are in frequence domain and standart deviation and correlation are in time-space domain.
\[\mathcal{F}=\{\mu,\epsilon,\delta,\sigma,\gamma\}\]

To obtain the features in frequency domain, we need to apply discrete Fourier transform (DFT) on each frame per axis to build a spectrum-based gesture representation.
\[t_{T,k}^m=\sum_{n=0}^{2L_S-1}r_{T,k}^n e^{-\frac{2\pi i}{2L_S-1}kn}\]
\[T=x,y,z\]
\[k = 0,1,\dots,N-1\]
\[m = 0,1,\dots,2L_S-1\]
After DFT, we can extract the three features in frequency domain as:
\[\mu_{T,k} = t_{T,k}^0\]
\[\epsilon_{T,k} = \frac{\sum_{n=1}^{2L_S-1}|t_{T,k}^n|^2}{|2L_S-1|}\]
We define the entropy as:
\[\delta_{T,k} = \sum_{m=1}^{2L_S-1}p_{T,k}^m\log\left(\frac{1}{p_{T,k}^m}\right)\]
\[p_{T,k}^m = \frac{|t_{T,k}^m|}{\sum_{n=1}^{2L_S-1}|t_{T,k}^n|}\]
In the time domain, we extract two features, namely standard deviation $\sigma$ and correlation $\gamma$. Standart deviation of a gesture measures the amplitude variability of a gesture, compared with the average value.
\[\sigma_{T,k} = \sqrt{\sum_{n=0}^{2L_S-1}(r_{T,k}^n-\bar{r}_{T,k}^n)^2}\]
\[p_{T,k}^m = \frac{|t_{T,k}^m|}{\sum_{n = 1}^{2L_S-1}|t_{T,k}^n|}\]
The correlation provides us with information of the strength of linearity between different axes:
\[\gamma_{T_1\sim T_2,k} = \frac{v_{T_1\sim T_2,k}^n-\bar{r}_{T_1,k}^n\cdot \bar{r}_{T_2,k}^n}{\sqrt{v_{T_1\sim T_1,k}^n-(\bar{r}_{T_1,k}^n)^2}\cdot \sqrt{v_{T_1\sim T_1,k}^n-(\bar{r}_{T_2,k}^n)^2}}\]
\[v_{T_1\sim T_2,k}^n = \frac{\sum_{n=0}^{2L_S-1}|r_{T_1,k}^n\cdot r_{T_2,k}^n|}{2L_S}\]
\[T_1,T_2 = x,y,z\]
So far, we obtain 5 features for each each frame of a gesture and we can use this as input data for SVM for training.
\subsection{SVM with NDTW Kernel}
DTW and SVM are both powerful tools for gesture recognition. So why not combine these two together? During our literature survey, we found that there are already some resarch works combining SVM and DTW~\cite{svmdtw08,svmdtw10}.

In~\cite{svmdtw08}, the authors propose a method combining SVM and DTW by using the NDTW kernel in SVM. NDTW refers to negated dynamic time warping and the kernel function is defined as:
\[k_{NDTW}(x,z) = -d_{DTW}(x,z)\]
The reason that we take the negative value is that we can regularize the matrix during the traing procedure by substracting the smallest eigenvalue from the diagonal while the test-train matrix is left unchanged.
\subsection{SVM with GDTW Kernel}
Another option for the kernel function is GDTW (Gaussian Dynamic Time Warping). GDTW kernel function is defined as:
\[k_{GDTW}(x,z) = \exp(-\gamma d_{DTW}(x,z))\]
where $\gamma > 0$ is a user-specified shape parameter. The kernel function is not positive definite but there are theoretical and empirical explanations to why it works well in practice~\cite{gdtw}.

\subsection{SVM with Preprocessing Data}
In uWave, before we use the data for DTW processing, we have a quantization phase to normalize the data. This normalization is also applicable to our design with SVM. For SVM with NDTW/GDTW kernel, we also apply the quantization before the actual training.

Another way to preprocessing data is scaling. Scaling in SVM is a very important tool. In our desing, we linearly scale the acceleration data to the range of $[-1,1]$. One one hand, this can significantly reduce the computation overheads. On the other hand, this can improve the sensitivity of SVM by avoiding the influence of erratic changes in accerleration data thus improve the final accuracy.
\section{Application}
\label{application}
We implemented GRACE on Android platform and the user interface is shown in Figure~\ref{ui}.
\begin{figure}
	\centering
	\includegraphics[width=0.6\linewidth]{ui.png}
	\caption{User Interface}
	\label{ui}
\end{figure}

We implemente the following functions in the application:
\begin{itemize}
	\item \textbf{Record Templates:} One significant advantage of GRACE is that we allow the user to self-define the gestures and can change them anytime. Upon pressing the ``Record'' button, the system enters the detecting mode and any movement of the phone will be recorded until the ``End'' button is pressed. Each recorded gesture can act as a template when we apply DTW to match the gesture with the template. By pressing ``clear'' button, all the gestures can be cleared.
	\item \textbf{Recognize Gestures:} After we record some templates in the system, we can do gesture recognition. Upon pressing the ``Recognize'' button, the system begins to detect the movement of the phone until ``End'' is pressed. Then by running the preloaded recognizing algorithm (DTW or SVM or others), the system will output the result as the index number of the matched gesture which already exists in the system.
	\item \textbf{Slide Control:} Besides the basic record and recognition function, we also develop an actual application of gesture recognition. The phone is connected to a computer by bluetooth. Once the system enters the slide control mode, user can hold the ``Record When Push'' button to do a hand gesture. When the button is released, the system can recognize the gesture and send a corresponding command to the computer by bluetooth to control the slides. In fact, the application is quite extensible which means that a lot of other applications can be developed based on the gesture recognition system.
	
\end{itemize}

\section{Evaluation}
\label{evaluation}
In evaluation, we adopt the most popular SVM toolkit libsvm which is a light weighted integrated software for SVM classification and regression.

%Evaluation for gesture recognition also requires large amount of data. To simplify the procedure of testing, we directly
For evaluation data, we conducted our experiments on the open-source data sets provided by Rice Efficient Computing Group which is also the group developed uWave~\cite{liu2009uwave}. In this data set, there are eight gestures shown as paths of hand movement in Figure \ref{gesture_vocabulary}. The gesture database is collected as follows. There are 8 users. Each user performed 8 hand gestures in different days. For each gesture, the user performed 10 times every day and repeats for 7 days.


\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{gesture_vocabulary.JPG}
	\caption{Gesture Vocabulary. The dot denotes the start and the arrow denotes the end}
	\label{gesture_vocabulary}
\end{figure}

 Therefore, for each gesture per user, we have 70 data points across seven days. The reason that we collect data in different days is that people usually perform the same gesture in a slightly different way across different days. Collecting data from different days can guarantee that the our data sets is fully diversified which can be furtherly reflected as the improvement of recognition accuracy.

We will introduce our evaluations in two experiments as same as in uWave~\cite{liu2009uwave} paper.
 
\subsection{Same User Different Days}
In this set of experiments, we compared the accuracy of different methods. For each gesture per user, we have total 70 data. We used one data point as the template or training data, all the other data act as testing data. For each user, we did 70 experiments, in i-th experiment, the i-th gesture data is the training data. Then, we got the average accuracy of 70 experiments. The accuracy of different methods is shown in Table~\ref{accuracy}

\begin{table}[!hbp]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		Name of the Method & Accuracy \\
		\hline
		uWave & 92.9\%\\
		\hline
		SVM with linear kernel & 83.8\%\\
		\hline
		FDSVM & 77.7\%\\
		\hline
		SVM with NDTW kernel & 92.4\%\\
		\hline
		SVM with GDTW kernel & 92.4\%\\
		\hline
		SVM+NDTW+Quantization & \textbf{94.1}\%\\
		\hline
		SVM+GDTW+Quantization & \textbf{94.1}\%\\
		\hline
		SVM+NDTW+Scale & 90.5\%\\
		\hline
		SVM+GDTW+Scale & 90.5\%\\
		\hline
	\end{tabular}
	\caption{Accuracy of different methods}
	\label{accuracy}
\end{table}
We can see that the performance of SVM with NDTW kernel and SVM with GDTW kernel are the same in all scenarios. Also, among all methods, the performance of SVM with NDTW/GDTW kernel with quantization is the best.
\subsection{Same User Same Day}
The other set of experiments we conducted is about our method with more training data. We used different fraction of data for training and comparing the consequent accuracy. Here we compare the performance of DTW in uWave with the performance of our SVM+NDTW+Quantization, which is actually the best performance GRACE can achieve. The results are shown in Figure~\ref{final}. From the performance, we could see our method got better result no matter with fewer or more training data.

\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{final.png}
	\caption{Accuracy vs. Overheads}
	\label{final}
\end{figure}
The experiment results show that SVM with NDTW kernel with quantization outperforms DTW of uWave in all situations. Therefore, we can conclude that with SVM+NDTW+ Quantization, GRACE can achieve high accuracy with low overheads.
\section{Conclusion}
\label{conclusion}
In this report, we introduced our work of GRACE, which is a gesture recognition system based on 3D accelerometer. We implemented 9 different methods for gesture recognition mainly based on DTW and SVM in GRACE and extensively evaluated the performance of differnt methods. By the evaluation results, we can prove that the with preprocessed input data, SVM with NDTW/GDTW kernel can provide the best result.


%
% The following two commands are all you need in the
% initial runs of your .tex file to
% produce the bibliography for the citations in your paper.
\nocite{*}
\bibliographystyle{abbrv}
\bibliography{sigproc}  % sigproc.bib is the name of the Bibliography in this case

% That's all folks!
\end{document}
